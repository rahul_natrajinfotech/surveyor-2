package com.natrajinfotech.propertysurvey.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.propertysurvey.Model.AssignPropertyDbModel
import com.natrajinfotech.propertysurvey.R
import kotlinx.android.synthetic.main.row_assign_properties.view.*

/**
 * Created by Dipendra Sharma  on 14-02-2019.
 */
class RvAssignPropertyAdaptor(var mContext:Context,var assignPropertyList:ArrayList<AssignPropertyDbModel>,var clickLisner:View.OnClickListener):RecyclerView.Adapter<RvAssignPropertyAdaptor.AssignPropertyViewholder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AssignPropertyViewholder {
return AssignPropertyViewholder(LayoutInflater.from(mContext).inflate(R.layout.row_assign_properties,p0,false))
    }

    override fun getItemCount(): Int {
return assignPropertyList.size
    }

    override fun onBindViewHolder(p0: AssignPropertyViewholder, p1: Int) {
        val assignData=assignPropertyList[p1]
p0.tvPropertyNumber.text=assignData.PropertyNumber
        p0.tvWard.text=assignData.WardName
        p0.tvView.tag=p1
        p0.tvAction.tag=p1
        p0.llAssignMain.tag=p1
        //p0.llAssignMain.setOnClickListener(clickLisner)
        p0.tvView.setOnClickListener(clickLisner)
        p0.tvAction.setOnClickListener(clickLisner)
    }


    class AssignPropertyViewholder(mView:View):RecyclerView.ViewHolder(mView){
val tvPropertyNumber=mView.tvPropertyNumber
        val tvWard=mView.tvWard
val tvView=mView.tvView
        val tvAction=mView.tvAction
val llAssignMain=mView.llAssignMain
    }
}