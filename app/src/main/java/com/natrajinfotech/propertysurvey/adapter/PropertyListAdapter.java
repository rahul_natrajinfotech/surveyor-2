package com.natrajinfotech.propertysurvey.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.natrajinfotech.propertysurvey.Model.CreateDataModel;
import com.natrajinfotech.propertysurvey.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PropertyListAdapter extends RecyclerView.Adapter<PropertyListAdapter.SingleItemRowHolder> implements Filterable {

    private ArrayList<CreateDataModel> dataList;
    private ArrayList<CreateDataModel> dataFilteredList;
    private Context mContext;
    private View.OnClickListener listener;


    public PropertyListAdapter(Context context, ArrayList<CreateDataModel> dataList, View.OnClickListener listener) {
        this.dataList = dataList;
        this.dataFilteredList = dataList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_property_list, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, final int i) {
        holder.tvSN.setText((i+1)+")");

        holder.tvPropertyNo.setText(dataFilteredList.get(i).getPropertyNo());
        holder.tvWard.setText(dataFilteredList.get(i).getWard());

        holder.llRoot.setTag(dataFilteredList.get(i));
        holder.llRoot.setOnClickListener(listener);

        if (dataFilteredList.get(i).getSynced()){
            holder.imgSyncStatus.setImageDrawable(mContext.getResources().getDrawable(R.drawable.success));
        }else {
            holder.imgSyncStatus.setImageDrawable(mContext.getResources().getDrawable(R.drawable.error));
        }

    }

    @Override
    public int getItemCount() {
        return (null != dataFilteredList ? dataFilteredList.size() : 0);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (dataList !=null && dataList.size()>0) {
                    if (charString.isEmpty()) {
                        dataFilteredList = dataList;
                    } else {
                        ArrayList<CreateDataModel> filteredList = new ArrayList<>();
                        for (CreateDataModel row : dataList) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getPropertyNo().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }

                        dataFilteredList = filteredList;
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataFilteredList = (ArrayList<CreateDataModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        private TextView tvSN, tvPropertyNo, tvWard;
        private ImageView imgSyncStatus;
        private LinearLayout llRoot;

        public SingleItemRowHolder(View view) {
            super(view);

            tvSN = (TextView) itemView.findViewById(R.id.tvSN);
            tvPropertyNo = (TextView) itemView.findViewById(R.id.tvPropertyNo);
            tvWard = (TextView) itemView.findViewById(R.id.tvWard);
            imgSyncStatus = (ImageView) itemView.findViewById(R.id.imgSyncStatus);
            llRoot = (LinearLayout) itemView.findViewById(R.id.llRoot);

        }

    }

   private String validateDate(String responseDate){
       DateFormat originalFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
       DateFormat targetFormat = new SimpleDateFormat("d MMM", Locale.ENGLISH);
       Date date = null;
       try {
           date = originalFormat.parse(responseDate);
       } catch (ParseException e) {
           e.printStackTrace();
       }
       return targetFormat.format(date);
   }

   public ArrayList<CreateDataModel> selectedDataList(){
        return dataList;
   }


}