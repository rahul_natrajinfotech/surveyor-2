package com.natrajinfotech.propertysurvey.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.propertysurvey.Model.FloorDetailDbModel
import com.natrajinfotech.propertysurvey.R
import kotlinx.android.synthetic.main.row_add_floor.view.*

/**
 * Created by Dipendra Sharma  on 14-02-2019.
 */
class RvFlorrDetailAdaptor(
    var mContext: Context,
    var floorList: List<FloorDetailDbModel>
):RecyclerView.Adapter<RvFlorrDetailAdaptor.FloorViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FloorViewHolder {
        return FloorViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_add_floor,p0,false))
    }

    override fun getItemCount(): Int {
        return floorList.size
    }

    override fun onBindViewHolder(p0: FloorViewHolder, p1: Int) {
        val floorData=floorList[p1]
val floorCount=p1+1
        p0.tvFloorCount.text=floorCount.toString()
        p0.tvFloorUses.text=floorData.usesType
        p0.tvConstructionType.text=floorData.constructionType
        p0.tvCarpetArea.text=floorData.carpetArea
    }


    class FloorViewHolder(mView:View):RecyclerView.ViewHolder(mView){
val tvFloorCount=mView.tvFloorCount
        val tvFloorUses=mView.tvFloorUsage
        val tvConstructionType=mView.tvConstructionType
        val tvCarpetArea=mView.tvCarpetArea


    }
}