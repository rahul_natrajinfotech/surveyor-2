package com.natrajinfotech.propertysurvey.Utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class NetworkHandler {

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        boolean isConnected = info != null && info.isConnected();
        if (!isConnected) {
            Toast.makeText(context, "There is no internet connection.", Toast.LENGTH_SHORT).show();
        }
        return isConnected;
    }
}
