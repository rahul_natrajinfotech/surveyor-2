package com.natrajinfotech.propertysurvey.Utility;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.natrajinfotech.propertysurvey.retrofit_provider.RetrofitApiClient;
import com.natrajinfotech.propertysurvey.retrofit_provider.RetrofitService;


public class BaseActivity extends AppCompatActivity {
    public RetrofitApiClient retrofitApiClient;
    public RetrofitApiClient retrofitRxClient;
    //public NetworkHandler networkHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //networkHandler = new NetworkHandler(this);
        retrofitRxClient = RetrofitService.getRxClient();
        retrofitApiClient = RetrofitService.getRetrofit();
    }
}
