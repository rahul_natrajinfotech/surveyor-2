package com.natrajinfotech.propertysurvey.retrofit_provider;


import android.app.Dialog;
import com.natrajinfotech.propertysurvey.BuildConfig;
import com.natrajinfotech.propertysurvey.Model.GetAssignPropertyListResponseModel;
import com.natrajinfotech.propertysurvey.Model.login.LoginModel;
import com.natrajinfotech.propertysurvey.Utility.AppProgressDialog;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

public class RetrofitService {

    private static RetrofitApiClient client;

    public static String BASE_URL = "http://kolbrogroup.com/MBMC-Mobile-App/";

    public RetrofitService() {

        HttpLoggingInterceptor mHttpLoginInterceptor = new HttpLoggingInterceptor();
        //here we will set our log level
        mHttpLoginInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder mOkClient = new OkHttpClient.Builder().readTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS).connectTimeout(300, TimeUnit.SECONDS);
        // add logging as last interceptor alwaysL̥
        // we will add login interceptor only in case of debug.
        if (BuildConfig.DEBUG) {
            mOkClient.addInterceptor(mHttpLoginInterceptor);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(mOkClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        client = retrofit.create(RetrofitApiClient.class);
    }

    public static RetrofitApiClient getRxClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();
        return retrofit.create(RetrofitApiClient.class);
    }

    public static RetrofitApiClient getRetrofit() {
        if (client == null)
            new RetrofitService();

        return client;
    }

    public static void getLoginData(final Dialog dialog, final Call<LoginModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getServerResponse(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

//for gettting assign property list
public static void getAssignPropertyList(final Dialog dialog, final Call<GetAssignPropertyListResponseModel> method, final WebResponse webResponse) {
    if (dialog != null)
        AppProgressDialog.show(dialog);

    method.enqueue(new Callback<GetAssignPropertyListResponseModel>() {
        @Override
        public void onResponse(Call<GetAssignPropertyListResponseModel> call, Response<GetAssignPropertyListResponseModel> response) {
            AppProgressDialog.hide(dialog);
            WebServiceResponse.handleResponse(response, webResponse);
        }

        @Override
        public void onFailure(Call<GetAssignPropertyListResponseModel> call, Throwable throwable) {
            AppProgressDialog.hide(dialog);
            webResponse.onResponseFailed(throwable.getMessage());
        }
    });
}




}