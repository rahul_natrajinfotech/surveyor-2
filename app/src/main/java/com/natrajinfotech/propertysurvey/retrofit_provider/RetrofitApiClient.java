package com.natrajinfotech.propertysurvey.retrofit_provider;


import com.natrajinfotech.propertysurvey.Model.GetAssignPropertyListResponseModel;
import com.natrajinfotech.propertysurvey.Model.login.LoginModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.Map;

public interface RetrofitApiClient {

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginModel> loginData(@Field("UserName") String UserName,
                               @Field("Password") String Password);

    @FormUrlEncoded
    @POST("Add_Property.php")
    Call<ResponseBody> uploadPropertyDataToServer(@Field("Image1") String Image1,
                                                  @Field("Image2") String Image2,
                                                  @Field("Image3") String Image3,
                                                  @Field("Image4") String Image4,

                                                  @Field("WardName") String WardName,
                                                  @Field("ZoneName") String ZoneName,
                                                  @Field("BlockNumber") String BlockNumber,
                                                  @Field("PropertyNumber") String PropertyNumber,
                                                  @Field("PropertyName") String PropertyName,
                                                  @Field("Address") String Address,

                                                  @Field("CorporationPropertyNumber") String CorporationPropertyNumber,
                                                  @Field("PropertyUsageType") String PropertyUsageType,
                                                  @Field("PropertyType") String PropertyType,
                                                  @Field("ResidentialCount") String ResidentialCount,
                                                  @Field("CommercialCount") String CommercialCount,
                                                  @Field("Remark1") String Remark1,
                                                  @Field("Remark2") String Remark2,
                                                  @Field("DeviceId") String DeviceId,

                                                  @Field("Longitude") String Longitude,
                                                  @Field("Latitude") String Latitude,
                                                  @Field("UserId") String UserId);
//for getting list of assign property list
    @FormUrlEncoded
    @POST("Assign_List.php")
 Call<GetAssignPropertyListResponseModel> getAssignPropertyList(@Field("UserId")String UserId);
    //for adding master data inserting.
    @Headers("Content-Type: application/json")
    @POST("Master_Data.php")
    Call<ResponseBody> uploadPropertyMasterDataToServer(@Body Map<String,Object> requestBody);

//for uploading floor data
    @FormUrlEncoded
    @POST("Insert_Property_Flat_Information.php")
Call<ResponseBody> uploadPropertyFloorDataToServer(@Field("PropertyId") String PropertyId,@Field("FloorNo") String FloorNo,@Field("RoomNo") String RoomNo,@Field("ConstructionType") String ConstructionType,@Field("UsesType") String UsesType,@Field("UsesSubType") String UsesSubType,@Field("YearOfFloorConstrucation") String YearOfFloorConstrucation,@Field("RentalValue") String RentalValue,@Field("CarpetArea") String CarpetArea,@Field("LegalStatus") String LegalStatus);


}