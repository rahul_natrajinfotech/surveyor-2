package com.natrajinfotech.propertysurvey.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.natrajinfotech.propertysurvey.Model.AssignPropertyDbModel;
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel;
import com.natrajinfotech.propertysurvey.Model.CreateDataModel;
import com.natrajinfotech.propertysurvey.Model.FloorDetailDbModel;
import com.natrajinfotech.propertysurvey.R;

import java.sql.SQLException;


/**
 * Created by Rahul on 8/31/2016.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "SurveyPropertyApp.db";
    private static final int DATABASE_VERSION = 1;
    public Dao<CreateDataModel, Integer> propertyDao;

    public DatabaseHelper(Context context) {
        super(context, "/mnt/sdcard/SurveyorApp/" + DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, CreateDataModel.class);
            TableUtils.createTable(connectionSource, AssignPropertyDbModel.class);
            TableUtils.createTable(connectionSource, BuildingDbModel.class);
            TableUtils.createTable(connectionSource, FloorDetailDbModel.class);

}    catch (SQLException var4) {
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, CreateDataModel.class, true);
            TableUtils.dropTable(connectionSource, AssignPropertyDbModel.class, true);
            TableUtils.dropTable(connectionSource, BuildingDbModel.class, true);
            TableUtils.dropTable(connectionSource, FloorDetailDbModel.class, true);

            this.onCreate(database, connectionSource);
        } catch (SQLException var6) {
        }

    }

    public Dao<CreateDataModel, Integer> getPropertyDao() throws SQLException {
        if (propertyDao== null) {
            propertyDao= getDao(CreateDataModel.class);
        }
        return propertyDao;
    }

}
