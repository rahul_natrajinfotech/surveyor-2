
package com.natrajinfotech.propertysurvey.Model;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class CreateDataModel implements Serializable {


    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true, columnName = "id")
    private Integer id;

    @DatabaseField(columnName = "synced")
    private Boolean synced = false;

    @DatabaseField(columnName = "ward")
    private String ward;

    @DatabaseField(columnName = "zone")
    private String zone;

    @DatabaseField(columnName = "block")
    private String block;

    @DatabaseField(columnName = "propertyNo")
    private String propertyNo;

    @DatabaseField(columnName = "propertyName")
    private String propertyName;

    @DatabaseField(columnName = "propertyAddress")
    private String propertyAddress;

    /************************************************************
    *************************************************************/

    @DatabaseField(columnName = "corporationPropertyNumber")
    private String corporationPropertyNumber;

    @DatabaseField(columnName = "propertyUsageType")
    private String propertyUsageType;

    @DatabaseField(columnName = "propertyType")
    private String propertyType;

    @DatabaseField(columnName = "residentialCount")
    private String residentialCount;

    @DatabaseField(columnName = "commercialCount")
    private String commercialCount;

    @DatabaseField(columnName = "remarkOne")
    private String remarkOne;

    @DatabaseField(columnName = "remarkTwo")
    private String remarkTwo;

    /************************************************************
     *************************************************************/

    @DatabaseField(columnName = "propertyImageOne")
    private String propertyImageOne;

    @DatabaseField(columnName = "propertyImageTwo")
    private String propertyImageTwo;

    @DatabaseField(columnName = "propertyImageThree")
    private String propertyImageThree;

    @DatabaseField(columnName = "propertyImageFour")
    private String propertyImageFour;

    @DatabaseField(columnName = "propertyLatitude")
    private Double propertyLatitude;

    @DatabaseField(columnName = "propertyLongitude")
    private Double propertyLongitude;

    public CreateDataModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSynced() {
        return synced;
    }

    public void setSynced(Boolean synced) {
        this.synced = synced;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getPropertyNo() {
        return propertyNo;
    }

    public void setPropertyNo(String propertyNo) {
        this.propertyNo = propertyNo;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyAddress() {
        return propertyAddress;
    }

    public String getCorporationPropertyNumber() {
        return corporationPropertyNumber;
    }

    public void setCorporationPropertyNumber(String corporationPropertyNumber) {
        this.corporationPropertyNumber = corporationPropertyNumber;
    }

    public String getPropertyUsageType() {
        return propertyUsageType;
    }

    public void setPropertyUsageType(String propertyUsageType) {
        this.propertyUsageType = propertyUsageType;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getResidentialCount() {
        return residentialCount;
    }

    public void setResidentialCount(String residentialCount) {
        this.residentialCount = residentialCount;
    }

    public String getCommercialCount() {
        return commercialCount;
    }

    public void setCommercialCount(String commercialCount) {
        this.commercialCount = commercialCount;
    }

    public String getRemarkOne() {
        return remarkOne;
    }

    public void setRemarkOne(String remarkOne) {
        this.remarkOne = remarkOne;
    }

    public String getRemarkTwo() {
        return remarkTwo;
    }

    public void setRemarkTwo(String remarkTwo) {
        this.remarkTwo = remarkTwo;
    }

    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    public String getPropertyImageOne() {
        return propertyImageOne;
    }

    public void setPropertyImageOne(String propertyImageOne) {
        this.propertyImageOne = propertyImageOne;
    }

    public String getPropertyImageTwo() {
        return propertyImageTwo;
    }

    public void setPropertyImageTwo(String propertyImageTwo) {
        this.propertyImageTwo = propertyImageTwo;
    }

    public String getPropertyImageThree() {
        return propertyImageThree;
    }

    public void setPropertyImageThree(String propertyImageThree) {
        this.propertyImageThree = propertyImageThree;
    }

    public String getPropertyImageFour() {
        return propertyImageFour;
    }

    public void setPropertyImageFour(String propertyImageFour) {
        this.propertyImageFour = propertyImageFour;
    }

    public Double getPropertyLatitude() {
        return propertyLatitude;
    }

    public void setPropertyLatitude(Double propertyLatitude) {
        this.propertyLatitude = propertyLatitude;
    }

    public Double getPropertyLongitude() {
        return propertyLongitude;
    }

    public void setPropertyLongitude(Double propertyLongitude) {
        this.propertyLongitude = propertyLongitude;
    }
}
