package com.natrajinfotech.propertysurvey.Model;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created by Dipendra Sharma  on 15-02-2019.
 */
public class AssignPropertyDbModel implements Serializable {

    @DatabaseField(columnName = "PropertyId")
    public String PropertyId="";
    @DatabaseField(columnName = "WardName")
    public String WardName="";
    @DatabaseField(columnName = "ZoneName")
    public String ZoneName="";
    @DatabaseField(columnName = "BlockNumber")
public                 String BlockNumber="";
    @DatabaseField(columnName = "PropertyNumber")
    public String PropertyNumber="";
    @DatabaseField(columnName = "PropertyName")
    public String PropertyName="";
    @DatabaseField(columnName = "Address")
    public String Address="";
    @DatabaseField(columnName = "Image1")
    public String Image1="";
    @DatabaseField(columnName = "Image2")
    public String Image2="";
    @DatabaseField(columnName = "Image3")
    public String Image3="";
    @DatabaseField(columnName = "Image4")
    public String Image4="";
    @DatabaseField(columnName = "Longitude")
    public String Longitude="";
    @DatabaseField(columnName = "Latitude")
    public String Latitude="";
    @DatabaseField(columnName = "SyncStatus")
    public String SyncStatus="";
    @DatabaseField(columnName = "SyncDate")
    public String SyncDate="";
    @DatabaseField(columnName = "UserId")
    public String UserId="";
    @DatabaseField(columnName = "CorporationPropertyNumber")
    public String CorporationPropertyNumber="";
    @DatabaseField(columnName = "PropertyUsageType")
    public String PropertyUsageType="";
    @DatabaseField(columnName = "PropertyType")
    public String PropertyType="";
    @DatabaseField(columnName = "ResidentialCount")
    public String ResidentialCount="";
    @DatabaseField(columnName = "CommercialCount")
    public String CommercialCount="";
    @DatabaseField(columnName = "Remark1")
    public String Remark1="";
    @DatabaseField(columnName = "Remark2")
    public String Remark2="";
    @DatabaseField(columnName = "SurveyComplete")
    public String SurveyComplete="";

    public AssignPropertyDbModel() {
    }
}
