package com.natrajinfotech.propertysurvey.Model;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Dipendra Sharma  on 16-02-2019.
 */
public class BuildingDbModel implements Serializable {
@DatabaseField(allowGeneratedIdInsert = true, generatedId = true, columnName = "id")
    public int id= 0;

    @DatabaseField(columnName = "synced")
    public boolean synced= false;
    @DatabaseField(columnName ="PropertyId" )
    public String propertyId="";

    @DatabaseField(columnName = "propertyType")
    public String propertyType = "";

    @DatabaseField(columnName = "wardNumber")
    public String wardNumber="";
    @DatabaseField(columnName = "zoneNumber")
    public String zoneNumber="";
    @DatabaseField(columnName = "blockNumber")
    public String blockNumber="";
    @DatabaseField(columnName = "electionWardNumber")
    public String electionWardNumber="";
    @DatabaseField(columnName = "villageName")
    public String villageName="";
    @DatabaseField(columnName = "plotNumber")
    public String plotNumber="";
    @DatabaseField(columnName = "societyName")
    public String societyName="";
    @DatabaseField(columnName = "buildingName")
    public String buildingName="";
    @DatabaseField(columnName = "existingPropertyNumber")
    public String existingPropertyNumber="";
    @DatabaseField(columnName = "newPropertyNumber")
    public String newPropertyNumber="";
    @DatabaseField(columnName = "surveyNumber")
    public String surveyNumber="";
    @DatabaseField(columnName = "address")
    public String address="";
    @DatabaseField(columnName = "pinCode")
    public String pinCode="";
    @DatabaseField(columnName = "landMark")
    public String landMark="";
    @DatabaseField(columnName = "OwnerFirstName")
    public String ownerFirstName="";
    @DatabaseField(columnName = "OwnerMiddleName")
    public String ownerMiddleName="";

    @DatabaseField(columnName = "OwnerLastName")
    public String ownerLastName="";
    @DatabaseField(columnName = "CategoryOfOwner")
    public String categoryOfOwner="";
    @DatabaseField(columnName = "AadharNoOfOwner")
    public String aadharNoOfOwner="";
    @DatabaseField(columnName = "OwnerMobile")
    public String ownerMobile="";
    @DatabaseField(columnName = "ownerEmail")
    public String ownerEmail="";
    @DatabaseField(columnName = "OccupeirFirstName")
    public String occupeirFirstName="";

    @DatabaseField(columnName = "OccupeirMiddleName")
    public String occupeirMiddleName="";

    @DatabaseField(columnName = "occupeirLastName")
    public String occupeirLastName="";
    @DatabaseField(columnName = "occupeirMobile")
    public String occupeirMobile="";
    @DatabaseField(columnName = "occupeirAadharNumber")
    public String occupeirAadharNumber="";
    @DatabaseField(columnName = "TenantFirstName")
    public String tenantFirstName="";

    @DatabaseField(columnName = "TenantMiddleName")
    public String tenantMiddleName="";
    @DatabaseField(columnName = "TenantLastName")
    public String tenantLastName="";
    @DatabaseField(columnName = "TenantMobile")
    public String tenantMobile="";
    @DatabaseField(columnName = "TenantAadharNumber")
    public String tenantAadharNumber="";
    @DatabaseField(columnName = "MonthlyRent")
    public String monthlyRent="";
    @DatabaseField(columnName = "EstablishmentName")
    public String establishmentName="";
@DatabaseField(columnName = "NoOfParkings")
public String noOfParkings="";

    @DatabaseField(columnName = "isGarden")
    public String isGarden="";
    @DatabaseField(columnName = "isPool")
    public String isPool="";
    @DatabaseField(columnName = "isClub")
    public String isClub="";
    @DatabaseField(columnName = "UsesType")
    public String usesType="";
    @DatabaseField(columnName = "ConstructionType")
    public String constructionType="";
    @DatabaseField(columnName = "TotalPlotArea")
    public String totalPlotArea="";

    @DatabaseField(columnName = "latitude")
    public String latitude="";
    @DatabaseField(columnName = "longitude")
    public String longitude="";
    @DatabaseField(columnName = "majorRoadName")
    public String majorRoadName="";
    @DatabaseField(columnName = "majorRoadWidth")
    public String majorRoadWidth="";
    @DatabaseField(columnName = "minorRoadName")
    public String minorRoadName="";
    @DatabaseField(columnName = "minorRoadWidth")
    public String minorRoadWidth="";
    @DatabaseField(columnName = "assessmentYear")
    public String assessmentYear="";
    @DatabaseField(columnName = "buildingAge")
    public String buildingAge="";
    @DatabaseField(columnName = "oldAssessmentValue")
    public String oldAssessmentValue="";
    @DatabaseField(columnName = "YearOfBuildinConstruction")
    public String yearOfBuildinConstruction="";
    @DatabaseField(columnName = "BuildingPermissionYear")
    public String buildingPermissionYear="";
    @DatabaseField(columnName = "LegalStatus")
    public String legalStatus="";

    @DatabaseField(columnName = "BuildingPermissionNo")
    public String buildingPermissionNo="";

    @DatabaseField(columnName = "isStp")
    public String isStp="";
    @DatabaseField(columnName = "isRainWaterSystem")
    public String isRainWaterSystem="";
    @DatabaseField(columnName = "isSolarSystem")
    public String isSolarSystem="";
    @DatabaseField(columnName = "numberOfToilet")
    public String numberOfToilet="";

    @DatabaseField(columnName = "waterConnection")
    public String waterConnection="";
    @DatabaseField(columnName = "pipeSize")
    public String pipeSize="";
    @DatabaseField(columnName = "isWaterMeter")
    public String isWaterMeter="";
    @DatabaseField(columnName = "isElectricityConnection")
    public String isElectricityConnection="";
    @DatabaseField(columnName = "isLift")
    public String isLift="";
    @DatabaseField(columnName = "isSepticTenk")
    public String isSepticTenk="";

    @DatabaseField(columnName = "septicTenkDimension")
    public String septicTenkDimension="";
    @DatabaseField(columnName = "isAdvertisementSpace")
    public String isAdvertisementSpace="";
    @DatabaseField(columnName = "TypeOfAdvertisment")
    public String typeOfAdvertisment="";
    @DatabaseField(columnName = "SurveyStatus")
    public String SurveyStatus="";

    @DatabaseField(columnName = "remark")
    public String remark="";
    @DatabaseField(columnName = "propertyImageOne")
    public String propertyImageOne="";
    @DatabaseField(columnName = "propertyImageTwo")
    public String propertyImageTwo="";
    @DatabaseField(columnName = "propertyImageThree")
    public String propertyImageThree="";
    @DatabaseField(columnName = "propertyImageFour")
    public String propertyImageFour="";
    @DatabaseField(columnName = "signetureImage")
    public String signetureImage="";

public ArrayList<FloorDetailDbModel> floorList=new ArrayList<>();


    public BuildingDbModel() {
    }

}
