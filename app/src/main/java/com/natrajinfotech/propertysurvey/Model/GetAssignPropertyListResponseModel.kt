package com.natrajinfotech.propertysurvey.Model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 14-02-2019.
 */
class GetAssignPropertyListResponseModel:Serializable {
    var status:Boolean=false
    var msg:String=""
    var Report:ArrayList<AssignPropertyDbModel>?=null



}