package com.natrajinfotech.propertysurvey.Model;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created by Dipendra Sharma  on 16-02-2019.
 */
public class FloorDetailDbModel implements Serializable {
    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true, columnName = "id")
    public int id = 0;
@DatabaseField(columnName = "isSync")
public boolean isSync=false;
    @DatabaseField(columnName = "PropertyId")
    public String propertyId="";
    @DatabaseField(columnName = "FloorNo")
    public  String floorNo= "";
    @DatabaseField(columnName = "RoomNo")
    public  String roomNo= "";
    @DatabaseField(columnName = "ConstructionType")
    public  String  constructionType="";
    @DatabaseField(columnName = "UsesType")
    public  String  usesType="";
    @DatabaseField(columnName = "UsesSubType")
    public  String  usesSubType="";
    @DatabaseField(columnName = "OccupantTypes")
    public  String  occupantTypes="";
    @DatabaseField(columnName = "YearOfFloorConstrucation")
    public String yearOfFloorConstrucation= "";
    @DatabaseField(columnName = "RentalValue")
    public String rentalValue="";
    @DatabaseField(columnName = "CarpetArea")
    public String carpetArea= "";
    @DatabaseField(columnName = "LegalStatus")
    public String legalStatus="";

    public FloorDetailDbModel() {
    }
}
