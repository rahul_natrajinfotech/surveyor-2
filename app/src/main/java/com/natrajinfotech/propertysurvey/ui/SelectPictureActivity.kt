package com.natrajinfotech.propertysurvey.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextPaint
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.Model.FloorDetailDbModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import com.natrajinfotech.propertysurvey.Utility.ImagePicker
import kotlinx.android.synthetic.main.activity_select_picture.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class SelectPictureActivity: AppCompatActivity() {

    private lateinit var mContext : Context
    private var propertyModel : BuildingDbModel? = null
    private val PICK_IMAGE_ID = 234
    var mCurrentPhotoPath: String? = null
    var photoFile: File? = null
    val REQUEST_TAKE_PHOTO = 1
    private var imageName = ""
    private val IMAGE_DIRECTORY = "/SurveyorApp"
    private var imagePosition = ""
    private var imgOneFile : File? = null
    private var imgTwoFile : File? = null
    private var imgThreeFile : File? = null
    private var imgFourFile : File? = null
    private var userId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_picture)
        mContext = this
        userId = AppPreference.getStringPreference(mContext, "userId")

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        if (intent!=null){
            propertyModel = intent.getSerializableExtra("PROPERTY_DATA") as BuildingDbModel?
            imageName = "Survey"+"${propertyModel!!.wardNumber}_${propertyModel!!.newPropertyNumber}_$userId"
        }

        ivSelectOne.setOnClickListener {
            imagePosition = "One"
            imageName ="Survey"+ "${propertyModel!!.wardNumber}_${propertyModel!!.newPropertyNumber}_${userId}_1"
            showSelectImageDialog()
        }
        ivSelectTwo.setOnClickListener {
            imagePosition = "Two"
            imageName ="Survey"+ "${propertyModel!!.wardNumber}_${propertyModel!!.newPropertyNumber}_${userId}_2"
            showSelectImageDialog()
        }
        ivSelectThree.setOnClickListener {
            imagePosition = "Three"
            imageName = "Survey"+"${propertyModel!!.wardNumber}_${propertyModel!!.newPropertyNumber}_${userId}_3"
            showSelectImageDialog()
        }
        ivSelectFour.setOnClickListener {
            imagePosition = "Four"
            imageName ="Survey"+ "${propertyModel!!.wardNumber}_${propertyModel!!.newPropertyNumber}_${userId}_4"
            showSelectImageDialog()
        }
        btnImageNext.setOnClickListener {
            validateData()
        }

    }

    private fun validateData() {

        if(imgOneFile!=null){
            propertyModel!!.propertyImageOne = imgOneFile!!.absolutePath
        }
        if(imgTwoFile!=null){
            propertyModel!!.propertyImageTwo = imgTwoFile!!.absolutePath
        }
        if(imgThreeFile!=null){
            propertyModel!!.propertyImageThree = imgThreeFile!!.absolutePath
        }
        if(imgFourFile!=null){
            propertyModel!!.propertyImageFour = imgFourFile!!.absolutePath
        }
        if (imgOneFile!=null || imgTwoFile!=null || imgThreeFile!=null || imgFourFile!=null) {

            var floorList=ArrayList<FloorDetailDbModel>()
val from:String=intent.getStringExtra("from")
            if (from.equals("floor")){
                floorList= (intent.getSerializableExtra("floorList") as ArrayList<FloorDetailDbModel>?)!!
            }
            startActivity(
                Intent(this@SelectPictureActivity, SignetureActivity::class.java)
                    .putExtra("PROPERTY_DATA", propertyModel)
                    .putExtra("floorList",floorList))
        }else{
            Toast.makeText(mContext, "You have to select at least one image", Toast.LENGTH_SHORT).show()
        }

    }


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            // Create the File where the photo should go
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                ex.printStackTrace()
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(this,
                    "com.natrajinfotech.propertysurvey.fileprovider", photoFile!!)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PICK_IMAGE_ID -> {
                    try{
                        val bitmap = ImagePicker.getImageFromResult(this, resultCode, data)
                        val path = saveImage(bitmap)
                        when (imagePosition) {
                            "One" -> {
                                ivAddOne.setImageBitmap(bitmap)
                                imgOneFile = path
                                ivSelectTwo.visibility = View.VISIBLE
                            }
                            "Two" -> {
                                ivAddTwo.setImageBitmap(bitmap)
                                imgTwoFile = path
                                ivSelectThree.visibility = View.VISIBLE
                            }
                            "Three" -> {
                                ivAddThree.setImageBitmap(bitmap)
                                imgThreeFile = path
                                ivSelectFour.visibility = View.VISIBLE
                            }
                            "Four" -> {
                                ivAddFour.setImageBitmap(bitmap)
                                imgFourFile = path
                            }
                        }
                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                    //mImageView.setImageBitmap(bitmap)
                }
                else -> {
                    try{
                        galleryAddPic()
                        when (imagePosition) {
                            "One" -> {
                                setPic(ivAddOne, imagePosition)
                                ivSelectTwo.visibility = View.VISIBLE
                            }
                            "Two" -> {
                                setPic(ivAddTwo, imagePosition)
                                ivSelectThree.visibility = View.VISIBLE
                            }
                            "Three" -> {
                                setPic(ivAddThree, imagePosition)
                                ivSelectFour.visibility = View.VISIBLE
                            }
                            "Four" -> {
                                setPic(ivAddFour, imagePosition)
                            }
                        }
                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }
            }
        }else{
            Toast.makeText(this@SelectPictureActivity, "No Image Selected", Toast.LENGTH_SHORT).show()
        }
    }

    private fun galleryAddPic() {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(mCurrentPhotoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        this.sendBroadcast(mediaScanIntent)
    }

    private fun setPic(mImageView: ImageView, imagePosition: String) {
        // Get the dimensions of the View
        val targetW = mImageView.width
        val targetH = mImageView.height

        // Get the dimensions of the bitmap
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        // Determine how much to scale down the image
        val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true

        val bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)
        //mImageView.scaleType = ImageView.ScaleType.FIT_CENTER
        mImageView.setImageBitmap(bitmap)
        when(imagePosition){
            "One" -> imgOneFile = saveImage(bitmap)
            "Two" -> imgTwoFile = saveImage(bitmap)
            "Three" -> imgThreeFile = saveImage(bitmap)
            "Four" -> imgFourFile = saveImage(bitmap)
        }
        //width 0.78  and height 1.29
    }

    private fun showSelectImageDialog() {
        val dialogBuilder = AlertDialog.Builder(mContext)
        dialogBuilder.setTitle("Select Image")
        dialogBuilder.setCancelable(false)

        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_select_image, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()

        val actionCamera = dialogView.findViewById<TextView>(R.id.tvCamera)
        val actionGallery = dialogView.findViewById<TextView>(R.id.tvGallery)
        val actionCancel = dialogView.findViewById<TextView>(R.id.tvCancel)


        actionCancel.setOnClickListener { alertDialog.dismiss() }
        actionCamera.setOnClickListener {
            dispatchTakePictureIntent()
            alertDialog.dismiss()
        }
        actionGallery.setOnClickListener {
            val chooseImageIntent = ImagePicker.getPickImageIntent(this)
            startActivityForResult(chooseImageIntent, PICK_IMAGE_ID)
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val imageFileName = timeStamp
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        /*val image = File.createTempFile(
                imageFileName, *//* prefix *//*
                ".png", *//* suffix *//*
                storageDir      *//* directory *//*
        )*/
        val image = File(storageDir, "$imageFileName.png")

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun saveImage(myBitmap: Bitmap): File {

        val markBitmap = mark(myBitmap, imageName, 30.0f, true)

        val bytes = ByteArrayOutputStream()
        markBitmap.compress(Bitmap.CompressFormat.JPEG, 91, bytes)
        val wallpaperDirectory = File(
            Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY)
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        val f = File(wallpaperDirectory, imageName+ Calendar.getInstance()
            .timeInMillis.toString() + ".jpg")
        try {
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                arrayOf(f.path),
                arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.absolutePath)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return f
    }


    /**
     *@param a set the alpha component [0..255] of the paint's color.
     * @param textSize set the paint's text size in pixel units.  Set the paint's text size. This value must be > 0
     */
    private fun mark(src: Bitmap, watermark: String, size: Float, underline: Boolean): Bitmap {
        val w = src.getWidth()
        val h = src.getHeight()
        val result = Bitmap.createBitmap(w, h, src.getConfig())

        val resources = mContext.getResources()
        val scale = resources.getDisplayMetrics().density

        val canvas = Canvas(result)
        val s = Rect(0, 0, w, h)
        val d = Rect(0, 0, w, h)
        canvas.drawBitmap(src, s, d, null)

        val paint = Paint()

        paint.setColor(Color.BLACK)
        paint.setAlpha(150)
        canvas.drawRect(0f,h-40f,w.toFloat(),h.toFloat(),paint)

        val textPaint = TextPaint()
        textPaint.setColor(Color.WHITE)
        textPaint.setAlpha(255)
        //textPaint.setTextSize(size)
        textPaint.setTextSize((14 * scale).toFloat())
        //textPaint.setTextAlign(Paint.Align.CENTER)
        textPaint.setAntiAlias(true)
        textPaint.setUnderlineText(false)

        /*paint.setColor(Color.WHITE)
        paint.setAlpha(255)
        paint.setTextSize(size)
        paint.setAntiAlias(true)
        paint.setUnderlineText(false)*/

        //val xPos = (canvas.getWidth() / 2)
        val xPos = (w - textPaint.getTextSize() * Math.abs(watermark.length / 2)) / 2
        val yPos = ((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2))

        /**
         * Draw the text, with origin at (x,y), using the specified paint. The origin is interpreted
         * based on the Align setting in the paint.
         *
         * @param text The text to be drawn
         * @param x The x-coordinate of the origin of the text being drawn
         * @param y The y-coordinate of the baseline of the text being drawn
         * @param paint The paint used for the text (e.g. color, size, style)
         */
        //canvas.drawText(watermark, 10.0f, h-10f, textPaint)
        //canvas.drawText(watermark, xPos, yPos, textPaint)
        canvas.drawText(watermark, xPos, h-10f, textPaint)

        return result
    }

    /*private Bitmap mark(Bitmap src, String watermark, Point location, Color color, int alpha, int size, boolean underline) {
    int w = src.getWidth();
    int h = src.getHeight();
    Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

    Canvas canvas = new Canvas(result);
    canvas.drawBitmap(src, 0, 0, null);

    Paint paint = new Paint();
    paint.setColor(color);
    paint.setAlpha(alpha);
    paint.setTextSize(size);
    paint.setAntiAlias(true);
    paint.setUnderlineText(underline);
    canvas.drawText(watermark, location.x, location.y, paint);

    return result;
}*/

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}
