package com.natrajinfotech.propertysurvey.ui

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.Dao
import com.natrajinfotech.propertysurvey.Database.DatabaseHelper
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.Model.FloorDetailDbModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import com.natrajinfotech.propertysurvey.Utility.AppProgressDialog
import com.natrajinfotech.propertysurvey.Utility.BaseActivity
import com.natrajinfotech.propertysurvey.Utility.NetworkHandler
import com.natrajinfotech.propertysurvey.firebase.Constants
import com.natrajinfotech.propertysurvey.retrofit_provider.RetrofitService
import com.natrajinfotech.propertysurvey.retrofit_provider.WebResponse
import kotlinx.android.synthetic.main.activity_contents_page.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.io.File
import java.io.IOException

class ContentPageActivity:BaseActivity(){
    private var doubleBackToExitPressedOnce = false
    private var dataList: List<BuildingDbModel> = ArrayList()
    lateinit var dataBaseHelper:DatabaseHelper
    lateinit var buildingDao:Dao<BuildingDbModel,Int>
    lateinit var floorDao:Dao<FloorDetailDbModel,Int>
    lateinit var mDialog:Dialog
    private var storageReference: StorageReference? = null
    var userId:String=""
    private var imageUrl = ""
    private var imgWeb1 = ""
    private var imgWeb2 = ""
    private var imgWeb3 = ""
    private var imgWeb4 = ""
private var imgWebSigneture=""
    lateinit var finalDataList:ArrayList<Map<String,Any>>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contents_page)
        userId = AppPreference.getStringPreference(this@ContentPageActivity, "userId")
        setSupportActionBar(toolbar)
        val actionBar=supportActionBar
        actionBar!!.title="Surveyor App"
        actionBar.setDisplayHomeAsUpEnabled(true)
// Method to create Directory, if the Directory doesn't exists
        val DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/SurveyorApp/"
        val file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }

        btnAssignProperty.setOnClickListener {
            startActivity(Intent(this@ContentPageActivity,AssignedPropertiesActivity::class.java))
            finish()

        }

        btnNumbering.setOnClickListener {
            startActivity(Intent(this,NumberingActivity::class.java))
        }
    btnSyncProperty.setOnClickListener {
        storageReference = FirebaseStorage.getInstance().reference
        mDialog = Dialog(this@ContentPageActivity)
        finalDataList=ArrayList()
        dataBaseHelper=OpenHelperManager.getHelper(this@ContentPageActivity,DatabaseHelper::class.java)
        buildingDao=dataBaseHelper.getDao(BuildingDbModel::class.java)
        floorDao=dataBaseHelper.getDao(FloorDetailDbModel::class.java)
performSync()
    }

    }

    private fun performSync() {
        dataList=buildingDao.queryForAll()
        if (dataList.size > 0) {
            AppProgressDialog.show(mDialog)
            prepareDataForServer(0)
        }else{
            Toast.makeText(this@ContentPageActivity,"No data for syncing...",Toast.LENGTH_LONG).show()

        }
    }

     private fun prepareDataForServer(position: Int) {
        var p = position
        val data = dataList.get(p)

            sendOnServer(data,  p)

    }


    private fun sendOnServer(data: BuildingDbModel, dataPosition: Int) {
        var img1File: File? = null
        var img2File: File? = null
        var img3File: File? = null
        var img4File: File? = null
        var imgSignetureFile: File? = null

        val img1Url = ""
        val img2Url = ""
        val img3Url = ""
        val img4Url = ""
        val imgSignetureUrl = ""

        val fileList = ArrayList<String>()

        val file1Name = data.propertyImageOne
        val file2Name = data.propertyImageTwo
        val file3Name = data.propertyImageThree
        val file4Name = data.propertyImageFour
        val fileSignetureName = data.signetureImage
Log.v("dip","File Name :"+file1Name)
        if (file1Name != null && !file1Name.isEmpty()) {
            img1File = File(file1Name)
            val filePath = Uri.fromFile(img1File)
            //fileList.add(filePath)
            fileList.add(file1Name)
            //uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }else{
            fileList.add("na")

        }
        if (file2Name != null && !file2Name.isEmpty()) {
            img2File = File(file2Name)
            val filePath = Uri.fromFile(img2File)
            fileList.add(file2Name)
            //fileList.add(filePath)
            // uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }else{
            fileList.add("na")

        }
        if (file3Name != null && !file3Name.isEmpty()) {
            img3File = File(file3Name)
            val filePath = Uri.fromFile(img3File)
            fileList.add(file3Name)
            //fileList.add(filePath)
            //uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }else{
            fileList.add("na")

        }
        if (file4Name != null && !file4Name.isEmpty()) {

            img4File = File(file4Name)
            val filePath = Uri.fromFile(img3File)
            fileList.add(file4Name)
            //fileList.add(filePath)
            //uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }else{
            fileList.add("na")

        }

        if (fileSignetureName!= null && !fileSignetureName.isEmpty()) {
            imgSignetureFile= File(fileSignetureName)
            val filePath = Uri.fromFile(imgSignetureFile)
            fileList.add(fileSignetureName)
            //fileList.add(filePath)
            //uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }else{
            fileList.add("na")

        }

        if (NetworkHandler.isNetworkAvailable(this@ContentPageActivity)) {
            uploadImageFile(
                data.wardNumber+ "_" + data.existingPropertyNumber+ "_" + userId + "_" + data.id + "_",
                fileList,
                0,

                dataPosition
            )
        } else {
            Toast.makeText(this@ContentPageActivity, "No Network", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@ContentPageActivity, ContentPageActivity::class.java))
            finish()
        }

    }

    private fun updateData() {
        try {
            for (i in dataList.indices){
                var data: BuildingDbModel?=dataList[i]
            val updateBuilder = buildingDao.updateBuilder()
            updateBuilder.updateColumnValue("synced", true)
            updateBuilder.where().eq("id", data!!!!.id)
        val     isUpdated = updateBuilder.update()
            Log.v("UPDATED", "update value :$isUpdated")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun uploadImageFile(
        img_name: String,
        fileList: List<String>,
        position: Int,

        dataPosition: Int
    ) {

        if (!fileList.isEmpty()) {
            val filePath = fileList[position]

            if (!filePath .equals("na")) {
                val imgFile= File(filePath)
                val filePathUri = Uri.fromFile(imgFile)

                val imgCount = position + 1

                val sRef = storageReference!!.child(Constants.STORAGE_PATH_UPLOADS + img_name + imgCount + "." + "jpg")

                sRef.putFile(filePathUri)
                    .addOnSuccessListener(OnSuccessListener<UploadTask.TaskSnapshot> { taskSnapshot ->
                        var pos = position
                        pos++

                        //Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                        imageUrl = taskSnapshot.downloadUrl!!.toString()
                        when (pos) {
                            1 -> imgWeb1 = imageUrl
                            2 -> imgWeb2 = imageUrl
                            3 -> imgWeb3 = imageUrl
                            4 -> imgWeb4 = imageUrl
                            5 -> imgWebSigneture = imageUrl
                        }
                        if (pos < fileList.size) {
                            uploadImageFile(img_name, fileList, pos, dataPosition)
                        } else {
                            updateFloorData(dataPosition)
                            //Toast.makeText(mContext, "All image Uploaded", Toast.LENGTH_SHORT).show();
                        }
                        Log.v("IMG_WEB_URI", imageUrl)
                    })
                    .addOnFailureListener(OnFailureListener { exception ->
                        //progressDialog.dismiss();
                        AppProgressDialog.show(mDialog)
                        startActivity(
                            Intent(
                                this@ContentPageActivity,
                                ContentPageActivity::class.java
                            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        )
                        finish()
                        Toast.makeText(applicationContext, exception.message, Toast.LENGTH_LONG).show()
                    })


            } else {
                var pos = position
                pos++


                imageUrl ="NA"
                when (pos) {
                    1 -> imgWeb1 = imageUrl
                    2 -> imgWeb2 = imageUrl
                    3 -> imgWeb3 = imageUrl
                    4 -> imgWeb4 = imageUrl
                    5 -> imgWebSigneture = imageUrl
                }
                if (pos < fileList.size) {
                    uploadImageFile(img_name, fileList, pos,  dataPosition)
                } else {
//                    uploadFinalDataToServer(isDone, dataPosition)
                    updateFloorData(dataPosition)

                }
            }
        }else{
            //uploadFinalDataToServer(isDone, dataPosition)

        }}

    private fun uploadFinalDataToServer() {
        val finalPropertyData=HashMap<String,Any>()
        finalPropertyData["Master_Data"]=finalDataList

        if (NetworkHandler.isNetworkAvailable(this@ContentPageActivity)) {

            RetrofitService.getServerResponse(Dialog(this@ContentPageActivity), retrofitApiClient.uploadPropertyMasterDataToServer(finalPropertyData
                            ), object : WebResponse {
                override fun onResponseSuccess(result: Response<*>) {
                    val response = result.body() as ResponseBody
                    try {
                        val jsonObject = JSONObject(response.string())
                        if (jsonObject.getBoolean("status")) {
                            Toast.makeText(this@ContentPageActivity, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show()
                            updateData()

                        } else {
                            Toast.makeText(this@ContentPageActivity, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this@ContentPageActivity, NumberingActivity::class.java))
                            finish()
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                        startActivity(Intent(this@ContentPageActivity, NumberingActivity::class.java))
                        finish()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        startActivity(Intent(this@ContentPageActivity, NumberingActivity::class.java))
                        finish()
                    }

                }

                override fun onResponseFailed(error: String) {
                    Toast.makeText(this@ContentPageActivity, error, Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@ContentPageActivity, NumberingActivity::class.java))
                    finish()
                }
            })
        } else {
            Toast.makeText(this, "No Network", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@ContentPageActivity, ContentPageActivity::class.java))
            finish()
        }


        //prepareDataForServer(dataPosition);
    }

    private fun updateFloorData( dataPosition: Int) {
val floorDataList=floorDao.queryForEq("PropertyId",dataList[dataPosition].propertyId)
        var floorFinalArray=ArrayList<Map<String,String>>()
        if (floorDataList.size!=0){
            var floorFinalArray=ArrayList<Map<String,String>>()
            for (i in floorDataList.indices){
                var floorDataMap=HashMap<String,String>()
                floorDataMap["PropertyId"]=floorDataList[i].propertyId
                floorDataMap["FloorNo"]=floorDataList[i].floorNo
floorDataMap["RoomNo"]=floorDataList[i].roomNo
                floorDataMap["ConstructionType"]=floorDataList[i].constructionType
                floorDataMap["UsesType"]=floorDataList[i].usesType
                floorDataMap["UsesSubType"]=floorDataList[i].usesSubType
                floorDataMap["YearOfFloorConstrucation"]=floorDataList[i].yearOfFloorConstrucation
                floorDataMap["RentalValue"]=floorDataList[i].rentalValue
                floorDataMap["CarpetArea"]=floorDataList[i].carpetArea
                floorDataMap["LegalStatus"]=floorDataList[i].legalStatus
                floorDataMap["OccupantTypes"]=floorDataList[i].occupantTypes
                floorFinalArray.add(floorDataMap)
            }
prepairPropertyMapData(dataPosition,floorFinalArray)

            //sendFloorDetailIntoServer(floorDataList,0,dataPosition)

        }else{
            prepairPropertyMapData(dataPosition,floorFinalArray)

        }
    }

    private fun prepairPropertyMapData(dataPosition: Int,floorFinalArray:ArrayList<Map<String,String>>) {
val propertyData=dataList[dataPosition]
        val finalDataMap=HashMap<String,Any>()
        finalDataMap["PropertyId"]=propertyData.propertyId
        finalDataMap["PropertyType"]=propertyData.propertyType
        finalDataMap["WardNo"]=propertyData.wardNumber
        finalDataMap["ZoneNo"]=propertyData.zoneNumber
        finalDataMap["BlockNo"]=propertyData.blockNumber
        finalDataMap["ElectionWardNo"]=propertyData.electionWardNumber
        finalDataMap["VillageName"]=propertyData.villageName
        finalDataMap["PlotNo"]=propertyData.plotNumber
        finalDataMap["SocietyName"]=propertyData.societyName
        finalDataMap["BuildingName"]=propertyData.buildingName
        finalDataMap["ExistingPropertyNo"]=propertyData.existingPropertyNumber
        finalDataMap["NewPropertyNo"]=propertyData.newPropertyNumber
        finalDataMap["SurveyNo"]=propertyData.surveyNumber
        finalDataMap["Address"]=propertyData.address
        finalDataMap["PinCode"]=propertyData.pinCode
        finalDataMap["NearestLandmark"]=propertyData.landMark
        finalDataMap["OwnerFirstName"]=propertyData.ownerFirstName
        finalDataMap["OwnerMiddleName"]=propertyData.ownerMiddleName
        finalDataMap["OwnerLastName"]=propertyData.ownerLastName
        finalDataMap["CategoryOfOwner"]=propertyData.categoryOfOwner
        finalDataMap["AadharNoOfOwner"]=propertyData.aadharNoOfOwner
        finalDataMap["MobileNo"]=propertyData.ownerMobile
        finalDataMap["EmailId"]=propertyData.ownerEmail
        finalDataMap["OccupierFirstName"]=propertyData.occupeirFirstName
        finalDataMap["OccupierSecondName"]=propertyData.occupeirMiddleName
        finalDataMap["OccupierLastName"]=propertyData.occupeirLastName
        finalDataMap["OccupierMobileNo"]=propertyData.occupeirMobile
        finalDataMap["OccupierAdhaarNo"]=propertyData.occupeirAadharNumber
        finalDataMap["TenantFirstName"]=propertyData.tenantFirstName
        finalDataMap["TenantMiddleName"]=propertyData.tenantMiddleName
        finalDataMap["TenantLastName"]=propertyData.tenantLastName
        finalDataMap["TenantMobileNo"]=propertyData.tenantMobile
        finalDataMap["TenantAdhaarNo"]=propertyData.tenantAadharNumber
        finalDataMap["MonthlyRent"]=propertyData.monthlyRent
        finalDataMap["EstablishmentName"]=propertyData.establishmentName
        finalDataMap["NoOfParkings"]=propertyData.noOfParkings
        finalDataMap["Garden"]=propertyData.isGarden
        finalDataMap["SwimmingPool"]=propertyData.isPool
        finalDataMap["ClubHouse"]=propertyData.isClub
        finalDataMap["UsesType"]=propertyData.usesType
        finalDataMap["ConstructionType"]=propertyData.constructionType
        finalDataMap["Longitude"]=propertyData.longitude
        finalDataMap["Latitude"]=propertyData.latitude
        finalDataMap["MajorRoadName"]=propertyData.majorRoadName
        finalDataMap["MajorRoadWidth"]=propertyData.majorRoadWidth
        finalDataMap["MinorRoadName"]=propertyData.minorRoadName
finalDataMap["MinorRoadWidth"]=propertyData.minorRoadWidth
        finalDataMap["AssessmentYear"]=propertyData.assessmentYear
        finalDataMap["AgeOfBuilding"]=propertyData.buildingAge
        finalDataMap["OldAssessmentValue"]=propertyData.oldAssessmentValue
        finalDataMap["YearOfBuildinConstruction"]=propertyData.yearOfBuildinConstruction
        finalDataMap["BuildingPermissionYear"]=propertyData.buildingPermissionYear
        finalDataMap["LegalStatus"]=propertyData.legalStatus
        finalDataMap["BuildingPermissionNo"]=propertyData.buildingPermissionNo
        finalDataMap["STP"]=propertyData.isStp
        finalDataMap["RainWaterHarwasting"]=propertyData.isRainWaterSystem
        finalDataMap["SolarUnit"]=propertyData.isSolarSystem
        finalDataMap["NoOfToilet"]=propertyData.numberOfToilet
        finalDataMap["WaterConnection"]=propertyData.waterConnection
        finalDataMap["PipeSize"]=propertyData.pipeSize
        finalDataMap["WaterMeter"]=propertyData.isWaterMeter
        finalDataMap["ElectricityConnection"]=propertyData.isElectricityConnection
        finalDataMap["Lift"]=propertyData.isLift
        finalDataMap["SepitcTank"]=propertyData.isSepticTenk
        finalDataMap["SepitcTankDimention"]=propertyData.septicTenkDimension
        finalDataMap["AdvertismentOnBuilding"]=propertyData.isAdvertisementSpace
        finalDataMap["TypeOfAdvertisment"]=propertyData.typeOfAdvertisment
        finalDataMap["SurveyStatus"]=propertyData.SurveyStatus
        finalDataMap["Remark"]=propertyData.remark
        finalDataMap["TotalPlotArea"]=propertyData.totalPlotArea
        finalDataMap["ImageLink1"]=imgWeb1
        finalDataMap["ImageLink2"]=imgWeb2
        finalDataMap["ImageLink3"]=imgWeb3
        finalDataMap["ImageLink4"]=imgWeb4
        finalDataMap["SignatureImageLink"]=imgWebSigneture
        finalDataMap["Flat_Information"]=floorFinalArray
        finalDataList.add(finalDataMap)
if (finalDataList.size==dataList.size){
    uploadFinalDataToServer()

}else{
    prepareDataForServer(dataPosition)

}

    }

    private fun sendFloorDetailIntoServer(floorDataList: List<FloorDetailDbModel>, floorPosition: Int,propertyCurrentPosition:Int) {
if (floorDataList.size==floorPosition){
    prepareDataForServer(propertyCurrentPosition)

}else{
callFloorDataApi(floorDataList,floorPosition,propertyCurrentPosition)

}


    }

    private fun callFloorDataApi(
        floorDataList: List<FloorDetailDbModel>,
        floorPosition: Int,
        propertyCurrentPosition: Int
    ) {
val propertyFloorData=floorDataList[floorPosition]
        if (NetworkHandler.isNetworkAvailable(this@ContentPageActivity)) {

            RetrofitService.getServerResponse(Dialog(this@ContentPageActivity), retrofitApiClient.uploadPropertyFloorDataToServer(propertyFloorData.propertyId,propertyFloorData.floorNo,propertyFloorData.roomNo,propertyFloorData.constructionType,propertyFloorData.usesType,propertyFloorData.usesSubType,propertyFloorData.yearOfFloorConstrucation,propertyFloorData.rentalValue,propertyFloorData.carpetArea,propertyFloorData.legalStatus), object : WebResponse {
                override fun onResponseSuccess(result: Response<*>) {
                    val response = result.body() as ResponseBody
                    try {
                        val jsonObject = JSONObject(response.string())
                        if (jsonObject.getBoolean("status")) {
                            Toast.makeText(this@ContentPageActivity, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show()
                            updatePropertyFloorData(propertyFloorData)

                            sendFloorDetailIntoServer(floorDataList,floorPosition+1,propertyCurrentPosition)
                            //prepareDataForServer(propertyCurrentPosition)
     } else {
                            Toast.makeText(this@ContentPageActivity, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this@ContentPageActivity, ContentPageActivity::class.java))
                            finish()
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                        startActivity(Intent(this@ContentPageActivity, NumberingActivity::class.java))
                        finish()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        startActivity(Intent(this@ContentPageActivity, NumberingActivity::class.java))
                        finish()
                    }

                }

                override fun onResponseFailed(error: String) {
                    Toast.makeText(this@ContentPageActivity, error, Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@ContentPageActivity, NumberingActivity::class.java))
                    finish()
                }
            })
        } else {
            Toast.makeText(this, "No Network", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@ContentPageActivity, ContentPageActivity::class.java))
            finish()
        }

    }

    private fun updatePropertyFloorData(propertyFloorData: FloorDetailDbModel) {
        try {

            val updateBuilder =floorDao.updateBuilder()
            updateBuilder.updateColumnValue("isSync", true)
            updateBuilder.where().eq("id", propertyFloorData.id)
            val isUpdated = updateBuilder.update()
            Log.v("UPDATED", "update value :$isUpdated")
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }


    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }


    /*************************************************************
     * ***********************************************************/


}