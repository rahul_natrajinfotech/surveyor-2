package com.natrajinfotech.propertysurvey.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import android.widget.Toast
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.Model.FloorDetailDbModel
import com.natrajinfotech.propertysurvey.R
import kotlinx.android.synthetic.main.activity_add_floor.*

class AddFloorActivity:AppCompatActivity(),RadioGroup.OnCheckedChangeListener,AdapterView.OnItemSelectedListener{
    private val constructionTypeArray = arrayListOf( "High RCC",  "RCC ", "Load Bearing",  "Tin-shed with brick wall", "Kucha",  "Wooden" )
    private val usesTypeArray = arrayOf( "Residential","Non-residential")
    private val usesSubTypeArray = arrayListOf( "Flat","Bungalow","Duplex","Stilt","Parking","Row House","Slum","Individual House","Attached Garage","Society Office",
                                        "Educational","Chawl","Open Plot","Shop","Factory","Industry","Office","Hotel","Dining hotel","Cinema Hall","Bank","Hospital",
                                         "Clinic","Advertisement board","Mobile tower","Open Plot")
    private val oldUsesTypeArray = arrayOf( "Residential","Non-residential","Mixed")
    private val oldConstTypeArray = arrayListOf( "RCC ", "Load Bearing",  "Tin-shed with brick wall", "Kutcha","Open Land" )
    private val statusTypeArray = arrayOf( "Completed","Pending","Door Lock")


    var propertyId:String=""
    var floorNo:String=""
    var roomNo:String=""
    var constructionType:String=""
    var usesType:String=""
    var usesSubType:String=""
    var occupantTypes:String=""
    var yearOfFloorConstrucation:String=""
var rentalValue:String=""
    var carpetArea:String=""
    var legalStatus:String=""
    lateinit var floorList:ArrayList<FloorDetailDbModel>
    lateinit var from:String
    lateinit var buildingData:BuildingDbModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_floor)

inItView()

    }

    private fun inItView() {
from=intent.getStringExtra("from")
        floorList= ArrayList()
buildingData= intent.getSerializableExtra("PROPERTY_DATA") as BuildingDbModel
        if (from.equals("add")){
            floorList= intent.getSerializableExtra("floorList") as ArrayList<FloorDetailDbModel>

        }
        setSpinnerData()
        onClickLisner()
    }

    private fun onClickLisner() {
        rgOccupantTypes.setOnCheckedChangeListener(this@AddFloorActivity)
rgLegalStatus.setOnCheckedChangeListener(this@AddFloorActivity)
        spnUsesType?.onItemSelectedListener =this@AddFloorActivity
        spnConstructionType?.onItemSelectedListener =this@AddFloorActivity
        spnUsesSubType?.onItemSelectedListener =this@AddFloorActivity



        btnFloorData.setOnClickListener {
            getData()
            floorList.add(saveData())
            startActivity(Intent(this,AddFloorMainActivity::class.java)
                .putExtra("floorList",floorList)
                .putExtra("PROPERTY_DATA",buildingData))
            finish()
        }

    }

    private fun saveData():FloorDetailDbModel {
        val saveDataModel=FloorDetailDbModel()
        saveDataModel.isSync=false
        saveDataModel.propertyId=buildingData.propertyId
        saveDataModel.floorNo=floorNo
        saveDataModel.roomNo=roomNo
        saveDataModel.constructionType=constructionType
        saveDataModel.usesType=usesType
        saveDataModel.usesSubType=usesSubType
        saveDataModel.occupantTypes=occupantTypes
        saveDataModel.yearOfFloorConstrucation=yearOfFloorConstrucation
        saveDataModel.rentalValue=rentalValue
        saveDataModel.carpetArea=carpetArea
        saveDataModel.legalStatus=legalStatus
saveDataModel
        return saveDataModel
    }

    private fun setSpinnerData() {
        val usesAdapter = ArrayAdapter(this,R.layout.spinner_list,usesTypeArray)
        spnUsesType?.adapter = usesAdapter

        usesAdapter.notifyDataSetChanged()

        val constructionAdapter = ArrayAdapter(this,R.layout.spinner_list,constructionTypeArray)
        spnConstructionType?.adapter =constructionAdapter

        constructionAdapter.notifyDataSetChanged()

        val usesSubAdapter = ArrayAdapter(this,R.layout.spinner_list,usesSubTypeArray)
        spnUsesSubType?.adapter = usesSubAdapter

        usesSubAdapter.notifyDataSetChanged()






    }



    private fun getData() {
        floorNo=etFloorNo.text.toString().trim()
        roomNo=etRoomNo.text.toString().trim()
        constructionType=constructionTypeArray[spnConstructionType.selectedItemPosition]
        usesType=usesTypeArray[spnUsesType.selectedItemPosition]
        usesSubType=usesSubTypeArray[spnUsesSubType.selectedItemPosition]
        yearOfFloorConstrucation=etYearOfFloorConstrucation.text.toString().trim()
        rentalValue=etRentalValue.text.toString().trim()
        carpetArea=etCarpetArea.text.toString().trim()


    }



    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
when(p0?.id) {
    //for ownership type radio group
    R.id.rgOccupantTypes-> {
        if (p1==R.id.rdSelf){ 
            occupantTypes="self"
        }else{
            occupantTypes="rental"
        }
        
    }
//for legal type status radio group
    R.id.rgLegalStatus -> {
    if (p1==R.id.rdLegal){
        legalStatus="legal"
    }else{
        legalStatus="unLegal"
    }

}


}
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
when (p1?.id) {
    R.id.spnUsesType-> {
        if (p2>0) {
            usesType = usesTypeArray[p2]
        }
    }
        R.id.spnUsesSubType-> {
            if (p2>0)
            {
                usesSubType = usesSubTypeArray[p2]

        }

        }
R.id.spnConstructionType-> {
    if (p2>0){

        constructionType = constructionTypeArray[p2]
    }


}
}



    }
//for checking valid input
    fun isValidInput():Boolean{
    if (floorNo.isEmpty()){
        showToast("Please enter floor number.")
        return false
    }else if (roomNo.isEmpty()){
        showToast("Please enter room number.")
        return false
    }else if (spnConstructionType.selectedItemPosition==0){
        showToast("Please select construction type.")
        return false
    }else if (spnUsesType.selectedItemPosition==0){
        showToast("Please select uses type.")
        return false
    }else if (spnUsesSubType.selectedItemPosition==0){
        showToast("Please select uses sub type.")
        return false
    }else if (occupantTypes.isEmpty()){
        showToast("Please select occupeint type.")
        return false
    }else if (yearOfFloorConstrucation.isEmpty()){
        showToast("Please enter year of floor construction.")
        return false
    }else if (rentalValue.isEmpty()){
        showToast("Please enter rental value.")
        return false
    }else if (carpetArea.isEmpty()){
        showToast("Please enter carpet area.")
        return false
    }else if (legalStatus.isEmpty()){
        showToast("Please select legal status.")
        return false
    }else{
        return true

    }


}
    private fun showToast(messageStr: String) {
        Toast.makeText(this@AddFloorActivity,messageStr, Toast.LENGTH_LONG).show()
    }

}


