package com.natrajinfotech.propertysurvey.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import android.widget.Toast
import com.natrajinfotech.propertysurvey.Model.AssignPropertyDbModel
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.R
import kotlinx.android.synthetic.main.activity_survey_form.*

class SurveyFormActivity:AppCompatActivity(),AdapterView.OnItemSelectedListener,View.OnClickListener,RadioGroup.OnCheckedChangeListener {




lateinit var mContext:Context

    private val propertyTypeArray = arrayListOf("Select Property Type", "Society","Government Sector","Public Sector","Hospital","Godown","Shops","Hotel","Dinning Hotel","Theatre",
                                    "Mangal Karyalay ","Bunglow","Row house ","Mobile Tower","Individual house","Bank","Industry","Open Land","Commercial Open Land")
    private val catOwnerTypeArray = arrayListOf("Select Owner Type","Self","Rented","Lease")
    private val usesTypeArray = arrayListOf("Select Usage Type","Residential","Commercial ","Mixed")
    private val constructionTypeArray = arrayListOf("Select Construction Type", "High RCC",  "RCC ", "Load Bearing",  "Tin-shed with brick wall", "Kutcha",  "Wooden" )
    private val legalTypeArray = arrayListOf("Select Legal Type","Legal","Illegal")
    private val adTypeArray= arrayListOf("Select Advertisment Type","Flex","Digital")
    private val surveyTypeArray = arrayListOf("Select Survey Type","Completed","Pending")

    private var  propertyType = ""
    private var propertyId: String=""




    private var wardNumber: String=""
    private var zoneNumber: String=""
    private var blockNumber: String=""
    private var electionWardNumber: String=""
    private var villageName: String=""
    private var plotNumber: String=""
    private var societyName: String=""
    private var buildingName: String=""
    private var existingPropertyNumber: String=""
    private var newPropertyNumber: String=""
    private var surveyNumber: String=""
    private var address: String=""
    private var pinCode: String=""
    private var landMark: String=""
    private var propertyOwnerFirstName: String=""
    private var propertyOwnerMiddleName: String=""
    private var propertyOwnerLastName: String=""
    private var  categoryOfOwner:String =""
    private var aadharCardNumberOfOwner: String=""
    private var ownerMobileNumber:String=""
    private var ownerEmail:String=""
    private var occupeirFirstName:String=""
    private var occupeirMiddleName:String=""
    private var occupeirLastName:String=""
    private var occupeirMobileNumber:String=""
    private var occupeirAadharNumber:String=""
    private var tenantFirstName:String=""
    private var tenantMiddleName:String=""
    private var tenantLastName:String=""
    private var tenantMobileNumber:String=""
    private var tenantAadharNumber:String=""
private var monthlyRent:String=""
    private var establishmentName:String=""
    private var noOfParkings:String=""
    private var isGarden: String=""
    private var isPool: String=""
    private var isClub: String=""
    private var  usesType :String=""
    private var  constructionType:String=""
    private var totalPlotArea:String=""
    private var latitude: String=""
    private var longitude: String=""
    private var majorRoadName: String=""
    private var majorRoadWidth: String=""
    private var minorRoadName: String=""
    private var minorRoadWidth: String=""
    private var assessmentYear: String=""
    private var buildingAge: String=""
    private var oldAssessmentValue: String=""
    private var buildingConstructionYear: String=""
    private var buildingConstructionPermissionYear: String=""
    private var  legalType:String = ""
    private var buildingPermissionNo:String=""
    private var isStp: String=""
    private var isRainWaterSystem: String=""
    private var isSolarSystem: String=""
    private var numberOfToilet: String=""
    private var waterConnection: String=""
    private var pipeSize: String=""
    private var isWaterMeter: String=""
    private var isElectricityConnection: String=""
    private var isLift: String=""
    private var isSepticTenk:String=""
    private var septicTenkDimension: String=""
    private var isAdvertisementSpace: String=""
    private var typeOfAdvertisment:String=""
    private var surveyStatus:String=""
    private var remark: String=""






var propertyPreviousData:AssignPropertyDbModel?=null
        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey_form)
mContext=this@SurveyFormActivity
            inItView()



        }

    private fun inItView() {
propertyPreviousData= intent.getSerializableExtra("propertyData") as AssignPropertyDbModel?
if (propertyPreviousData!=null){
    setPropertyPreviousData()

}
        setSpinnerData()
rgGarden.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgAdvertisement.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgClub.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgElectricityConnection.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgLift.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgPool.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgRainWater.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgSepticTenk.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgSolarSystem.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgStp.setOnCheckedChangeListener(this@SurveyFormActivity)
        rgWaterMeter.setOnCheckedChangeListener(this@SurveyFormActivity)

        btnAddFloor.setOnClickListener {

            getData()

            startActivity(Intent(this,AddFloorActivity::class.java)
                .putExtra("PROPERTY_DATA",saveIntoDb())
                .putExtra("from","new"))
            finish()

        }

        btnSaveForm.setOnClickListener {
            getData()
            startActivity(Intent(this,SelectPictureActivity::class.java)
                .putExtra("PROPERTY_DATA",saveIntoDb())
                .putExtra("from","building"))
            finish()
        }
    }

    private fun setPropertyPreviousData() {
        propertyId= propertyPreviousData!!!!.PropertyId
        spnPT.setSelection(propertyTypeArray.indexOf(propertyPreviousData!!.PropertyType))
        etWardNumber.setText(propertyPreviousData!!.WardName)
        etZoneNumber.setText(propertyPreviousData!!.ZoneName)
        etBlockNumber.setText(
            propertyPreviousData!!.BlockNumber)
        etAddress.setText(propertyPreviousData!!.Address)
tvLat.text= propertyPreviousData!!.Latitude
        tvLng.text= propertyPreviousData!!.Longitude

    }


    private fun saveIntoDb():BuildingDbModel {
        val insertModel=BuildingDbModel()
insertModel.synced=false
        insertModel.propertyId=propertyId
        insertModel.propertyType=propertyType
        insertModel.wardNumber=wardNumber
        insertModel.zoneNumber=zoneNumber
        insertModel.blockNumber=blockNumber
        insertModel.electionWardNumber=electionWardNumber
        insertModel.villageName=villageName
        insertModel.plotNumber=plotNumber
        insertModel.societyName=societyName
        insertModel.buildingName=buildingName
        insertModel.existingPropertyNumber=existingPropertyNumber
        insertModel.newPropertyNumber=newPropertyNumber
        insertModel.surveyNumber=surveyNumber
        insertModel.address=address
        insertModel.pinCode=pinCode
        insertModel.landMark=landMark
        insertModel.ownerFirstName=propertyOwnerFirstName
        insertModel.ownerMiddleName=propertyOwnerMiddleName
        insertModel.ownerLastName=propertyOwnerLastName
insertModel.categoryOfOwner=categoryOfOwner
        insertModel.aadharNoOfOwner=aadharCardNumberOfOwner
        insertModel.ownerMobile=ownerMobileNumber
        insertModel.ownerEmail=ownerEmail
insertModel.occupeirFirstName=occupeirFirstName
        insertModel.occupeirMiddleName=occupeirMiddleName
        insertModel.occupeirLastName=occupeirLastName
        insertModel.occupeirMobile=occupeirMobileNumber
        insertModel.occupeirAadharNumber=occupeirAadharNumber
        insertModel.tenantFirstName=tenantFirstName
        insertModel.tenantMiddleName=tenantMiddleName
        insertModel.tenantLastName=tenantLastName
        insertModel.tenantMobile=tenantMobileNumber
        insertModel.tenantAadharNumber=tenantAadharNumber
        insertModel.monthlyRent=monthlyRent
        insertModel.establishmentName=establishmentName
        insertModel.noOfParkings=noOfParkings
        insertModel.isGarden=isGarden
insertModel.isPool=isPool
        insertModel.isClub=isClub
        insertModel.usesType=usesType
        insertModel.constructionType=constructionType
        insertModel.totalPlotArea=totalPlotArea
        insertModel.latitude=latitude
        insertModel.longitude=longitude
        insertModel.majorRoadName=majorRoadName
        insertModel.majorRoadWidth=majorRoadWidth
        insertModel.minorRoadName=minorRoadName
        insertModel.minorRoadWidth=minorRoadWidth
        insertModel.assessmentYear=assessmentYear
        insertModel.buildingAge=buildingAge
        insertModel.oldAssessmentValue=oldAssessmentValue
insertModel.yearOfBuildinConstruction=buildingConstructionYear
        insertModel.buildingPermissionYear=buildingConstructionPermissionYear
        insertModel.legalStatus=legalType
        insertModel.buildingPermissionNo=buildingPermissionNo
        insertModel.isStp=isStp
        insertModel.isRainWaterSystem=isRainWaterSystem
        insertModel.isSolarSystem=isSolarSystem
        insertModel.numberOfToilet=numberOfToilet
        insertModel.waterConnection=waterConnection
        insertModel.pipeSize=pipeSize
        insertModel.isWaterMeter=isWaterMeter
        insertModel.isElectricityConnection=isElectricityConnection
        insertModel.isLift=isLift
        insertModel.isSepticTenk=isSepticTenk
        insertModel.septicTenkDimension=septicTenkDimension
        insertModel.isAdvertisementSpace=isAdvertisementSpace
        insertModel.typeOfAdvertisment=typeOfAdvertisment
        insertModel.SurveyStatus=surveyStatus
        insertModel.propertyImageOne=""
        insertModel.propertyImageTwo=""
        insertModel.propertyImageThree=""
        insertModel.propertyImageFour
        insertModel.signetureImage=""
        insertModel.remark=remark
        return insertModel
    }


    private fun getData() {
        wardNumber=etWardNumber.text.toString().trim()
        zoneNumber=etZoneNumber.text.toString().trim()
        blockNumber=etBlockNumber.text.toString().trim()
electionWardNumber=etEWNumber.text.toString().trim()
        villageName=etVillageName.text.toString().trim()
        plotNumber=etPlotNumber.text.toString().trim()
        societyName=etSocietyName.text.toString().trim()
        buildingName=etBName.text.toString().trim()
        existingPropertyNumber=etEPNumber.text.toString().trim()
        newPropertyNumber=etNPNumber.text.toString().trim()
        surveyNumber=etSNumber.text.toString().trim()
    address=etAddress.text.toString().trim()
        pinCode=etPinCode.text.toString().trim()
        landMark=etLandmark.text.toString().trim()
        propertyOwnerFirstName=ettPOwnerFirstName.text.toString().trim()
        propertyOwnerMiddleName=ettPOwnerMiddleName.text.toString().trim()
        propertyOwnerLastName=ettPOwnerLastName.text.toString().trim()
        aadharCardNumberOfOwner=etAddharNo.text.toString().trim()
        ownerMobileNumber=etOwnerMobile.text.toString().trim()
        ownerEmail=etOwnerEmail.text.toString().trim()
        occupeirFirstName=etOccupierFirstName.text.toString().trim()
        occupeirMiddleName=etOccupierMiddleName.text.toString().trim()
        occupeirLastName=etOccupierLastName.text.toString().trim()
        occupeirMobileNumber=etOccupierMobileNumber.text.toString().trim()
        occupeirAadharNumber=etOccupierAadharNumber.text.toString().trim()
        tenantFirstName=etTenantFirstName.text.toString().trim()
        tenantMiddleName=etTenantMiddleName.text.toString().trim()
        tenantLastName=etTenantLastName.text.toString().trim()
        tenantMobileNumber=etTenantMobileNumber.text.toString().trim()
        tenantAadharNumber=etTenantAadharNumber.text.toString().trim()
        monthlyRent=etMonthlyRent.text.toString().trim()
        establishmentName=etEstablishmentName.text.toString().trim()
        noOfParkings=etNoOfParkings.text.toString().trim()
        totalPlotArea=etTotalPlotArea.text.toString().trim()
        latitude=tvLat.text.toString().trim()
        longitude=tvLng.text.toString().trim()
        majorRoadName=etMajorRoadName.text.toString().trim()
        majorRoadWidth=etMajorRoadWidth.text.toString().trim()
        minorRoadName=etMinorName.text.toString().trim()
    minorRoadWidth=etMinorWidth.text.toString().trim()
        assessmentYear=etAssessYear.text.toString().trim()
        buildingAge=etAgeOfBuilding.text.toString().trim()
        oldAssessmentValue=etOldAssessmentValue.text.toString().trim()
        buildingConstructionYear=etYearOfBuildinConstruction.text.toString()
        buildingConstructionPermissionYear=etPermissionYear.text.toString().trim()
        buildingPermissionNo=etBPNumber.text.toString().trim()
numberOfToilet=etNOToilet.text.toString().trim()
        waterConnection=etWConnection.text.toString().trim()
        pipeSize=etPipeSize.text.toString().trim()
septicTenkDimension=etSDimension.text.toString().trim()
remark=etRemark.text.toString().trim()
    }

    private fun setSpinnerData() {
        val propertyTypeAdapter = ArrayAdapter(this,R.layout.spinner_list,propertyTypeArray)
        spnPT?.adapter = propertyTypeAdapter
        spnPT?.onItemSelectedListener=this@SurveyFormActivity
        propertyTypeAdapter.notifyDataSetChanged()

        val categoryTypeAdapter = ArrayAdapter(this,R.layout.spinner_list,catOwnerTypeArray)
        spnCOT?.adapter = categoryTypeAdapter
        spnCOT?.onItemSelectedListener =this@SurveyFormActivity
        categoryTypeAdapter.notifyDataSetChanged()

        val usesTypeAdapter = ArrayAdapter(this,R.layout.spinner_list,usesTypeArray)
        spnUT?.adapter = usesTypeAdapter
        spnUT?.onItemSelectedListener =this@SurveyFormActivity
        usesTypeAdapter.notifyDataSetChanged()

        val constTypeAdapter = ArrayAdapter(this,R.layout.spinner_list,constructionTypeArray)
        spnCType?.adapter = constTypeAdapter
        spnCType?.onItemSelectedListener =this@SurveyFormActivity
        constTypeAdapter.notifyDataSetChanged()

        val legalTypeAdapter = ArrayAdapter(this,R.layout.spinner_list,legalTypeArray)
        spnLStatus?.adapter = legalTypeAdapter
        spnLStatus?.onItemSelectedListener =this@SurveyFormActivity
        legalTypeAdapter.notifyDataSetChanged()

        val adTypeAdapter = ArrayAdapter(this,R.layout.spinner_list,adTypeArray)
        spnAdType?.adapter = adTypeAdapter
        spnAdType?.onItemSelectedListener =this@SurveyFormActivity
        adTypeAdapter.notifyDataSetChanged()

        val surveyTypeAdapter = ArrayAdapter(this,R.layout.spinner_list,surveyTypeArray)
        spnSStatus?.adapter = surveyTypeAdapter
        spnSStatus?.onItemSelectedListener =this@SurveyFormActivity
        surveyTypeAdapter.notifyDataSetChanged()

    }
    override fun onClick(p0: View?) {
when (p0?.id) {


}
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
when (p0?.id) {
R.id.spnPT -> {
    if (p2>0) {
        propertyType= propertyTypeArray[p2]
    }

}
R.id.spnCOT -> {
    if (p2>0)
    {
        categoryOfOwner= catOwnerTypeArray[p2]
    }

}
R.id.spnUT -> {
    if (p2>0)
    {

        usesType= usesTypeArray[p2]
    }

}
R.id.spnCType -> {
    if (p2>0)
    {


        constructionType= constructionTypeArray[p2]
    }

}
R.id.spnLStatus -> {
    if (p2>0)
    {

        legalType = legalTypeArray[p2]
    }

}
R.id.spnAdType -> {
    if (p2>0)
    {

        typeOfAdvertisment= adTypeArray[p2]
    }

}
R.id.spnSStatus -> {
    if (p2>0)
    {

        surveyStatus= surveyTypeArray[p2]
    }

}
}
    }



    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
        when (p0?.id) {
            //for garden yes no radio button.
            R.id.rgGarden -> {
                if (p1 == R.id.rdGYes) {
                    isGarden = "Yes"

                } else {
                    isGarden = "No"
                }

            }
//for pool yes no radio button
            R.id.rgPool -> {
                if (p1 == R.id.rdPoolYes) {
                    isPool = "Yes"

                } else {
                    isPool = "No"
                }
            }
//for club yes no radio button.
            R.id.rgClub -> {
                if (p1 == R.id.rdClubYes) {
                    isClub = "Yes"
                } else {
                    isClub = "No"
                }
            }
//for stp radio yes no.
            R.id.rgStp -> {
                if (p1 == R.id.rdStpYes) {
                    isStp = "yes"
                } else {
                    isStp = "No"
                }

            }
//for rain water system yes no radio.
            R.id.rgRainWater -> {
                if (p1 == R.id.rdRWHYes) {
                    isRainWaterSystem = "Yes"
                } else {
                    isRainWaterSystem = "No"
                }

            }
//for solar system radio.
            R.id.rgSolarSystem -> {
                if (p1 == R.id.rdSolarYes) {
                    isSolarSystem = "Yes"
                } else {
                    isSolarSystem = "No"
                }

            }
            //for water meter radio.
            R.id.rgWaterMeter -> {
                if (p1 == R.id.rdWMYes) {
                    isWaterMeter = "Yes"
                } else {
                    isWaterMeter = "No"
                }

            }
            //for Electricity connection.
            R.id.rgElectricityConnection -> {
                if (p1 == R.id.rdECYes) {
                    isElectricityConnection = "Yes"
                } else {
                    isElectricityConnection = "No"
                }

            }
            //for lift.
            R.id.rgLift -> {
                if (p1 == R.id.rdLiftYes) {
                    isLift = "Yes"
                } else {
                    isLift = "No"
                }
            }
            //for septic tenk radio
            R.id.rgSepticTenk -> {
                if (p1 == R.id.rdSepticYes) {
                    isSepticTenk = "Yes"
                    etSDimension.visibility = View.VISIBLE
                } else {
                    isSepticTenk = "No"
                    etSDimension.visibility = View.GONE
                }

            }
//for advertisement on building radio.
            R.id.rgAdvertisement -> {
                if (p1 == R.id.rdAdYes) {
                    isAdvertisementSpace = "Yes"
                } else {
                    isAdvertisementSpace = "No"
                }

            }
        }

    }

//for checking Mandatory field
    private fun isValidInput():Boolean{
    if (propertyType.isEmpty()){
        showToast("Please Select Property Type")
        return false
    }else if (wardNumber.isEmpty()){
        showToast("Please enter ward number")
        return false
    }else if (zoneNumber.isEmpty()){
        showToast("Please enter zone number.")
return false
    }else if (villageName.isEmpty()){
        showToast("Please enter village name.")
        return false
    }else if (newPropertyNumber.isEmpty()) {
        showToast("Please enter New Property Number")
return false
    }else if (address.isEmpty()) {
        showToast("Please enter address.")
return false
    }else if (pinCode.isEmpty()){
        showToast("Please enter pincode.")
        return false
    }else if (landMark.isEmpty()){
        showToast("Please enter Nearest Landmark")
        return false
    }else if (propertyOwnerFirstName.isEmpty()){
        showToast("Please enter property owner first name.")
        return false
    }else if (propertyOwnerLastName.isEmpty()){
        showToast("Please enter Property owner last name")
return false
    }else if (ownerMobileNumber.isEmpty()){
        showToast("Please enter property owner Mobile number")
        return false
    }else if (occupeirFirstName.isEmpty()){
        showToast("Please enter occupeir first name.")
        return false
    }else if (occupeirLastName.isEmpty()){
        showToast("Please enter occupeir last name.")
        return false
    }else if (occupeirMobileNumber.isEmpty()){
        showToast("Please enter occupeir mobile number.")
        return false
    }else if (tenantFirstName.isEmpty()){
        showToast("Please enter tenant first name.")
        return false
    }else if (tenantLastName.isEmpty()){
        showToast("Please enter tenant last name.")
        return false
    }else if (tenantMobileNumber.isEmpty()){
        showToast("Please enter tenant mobile number.")
        return false
    }else if (isGarden.isEmpty()){
        showToast("Please select garden status.")
        return false
    }else if (isPool.isEmpty()){
        showToast("Please select SwimmingPool status.")
return false
    }else if (isClub.isEmpty()){
        showToast("Please select club house status.")
        return false
    }else if (usesType.isEmpty()){
        showToast("Please select Usage type.")
return false
    }else if (constructionType.isEmpty()){
        showToast("Please select construction type.")
        return false
    }else if (latitude.isEmpty()){
        showToast("Please enter latitude.")
        return false
    }else if (longitude.isEmpty()){
        showToast("Please enter longitude")
        return false
    }else if (buildingAge.isEmpty()){
        showToast("Please enter Bilding age.")
return false
    }else if (isStp.isEmpty()){
        showToast("Please select Stp.")
        return false
    }else if (isRainWaterSystem.isEmpty()){
        showToast("Please select RainWaterHarwasting status.")
return false
    }else if (isSolarSystem.isEmpty()){
        showToast("Please select SolarSystem status.")
        return false
    }else if (numberOfToilet.isEmpty()){
        showToast("Please enter Number of toilets.")
        return false
    }else if (waterConnection.isEmpty()){
        showToast("Please enter water connection detail.")
        return false
    }else if (isLift.isEmpty()){
        showToast("Please select lift status.")
        return false
    }else if (isSepticTenk.isEmpty()){
        showToast("Please select SepticTenk status.")
        return false
    }else{
        return true

    }



}

    private fun showToast(messageStr: String) {
        Toast.makeText(this@SurveyFormActivity,messageStr,Toast.LENGTH_LONG).show()
    }


}