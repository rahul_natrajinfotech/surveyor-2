package com.natrajinfotech.propertysurvey.ui

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Location
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextPaint
import android.util.Log
import android.view.View
import android.widget.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.Dao
import com.natrajinfotech.propertysurvey.Database.DatabaseHelper
import com.natrajinfotech.propertysurvey.MainActivity
import com.natrajinfotech.propertysurvey.Model.CreateDataModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import com.natrajinfotech.propertysurvey.Utility.AppUtils
import com.natrajinfotech.propertysurvey.Utility.ImagePicker
import com.natrajinfotech.propertysurvey.Utility.ScrollGoogleMap
import kotlinx.android.synthetic.main.activity_edit_property_detail.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.sql.SQLException
import java.text.SimpleDateFormat
import java.util.*

class EditPropertyDetailActivity : AppCompatActivity(), OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private var mMap: GoogleMap? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
    private val TAG = "MAP LOCATION"
    internal lateinit var mContext: Context
    private var data: CreateDataModel? = null
    internal lateinit var mLocationMarkerText: TextView
    private var finalLat = 0.0
    private var finalLong = 0.0
    private var mCenterLatLong: LatLng? = null

    private val PICK_IMAGE_ID = 234
    var mCurrentPhotoPath: String? = null
    var photoFile: File? = null
    val REQUEST_TAKE_PHOTO = 1
    private var imageName = ""
    private val IMAGE_DIRECTORY = "/SurveyorApp"
    private var imagePosition = ""
    private var imgOneFile: File? = null
    private var imgTwoFile: File? = null
    private var imgThreeFile: File? = null
    private var imgFourFile: File? = null
    private var userId: String? = null

    private val propertyUsageTypeArray = arrayOf("Select a Property Usage Type", "RESIDENTIAL", "COMMERCIAL", "MIXED", "INDUSTRIAL")
    private val wardNameArray = arrayOf("Select a Ward", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T")
    private val propertyTypeArray = arrayOf("Select a Property Type", "APARTMENT", "CHAWL", "SLUM", "BUNGALOW", "INDIVIDUAL HOUSE",
            "ROW HOUSE", "SHOP", "OFFICE", "OTHERS", "MIXED", "INDUSTRIAL")

    private var propertyUsageType = ""
    private var propertyType = ""
    private var wardName = ""

    // Reference of DatabaseHelper class to access its DAOs and other components
    private var ormLiteDb : DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_property_detail)
        mContext = this
        userId = AppPreference.getStringPreference(mContext, "userId")

        this.title = "Update"
        showHomeBackOnActionbar()

        data = intent.getSerializableExtra("DATA_MODEL") as CreateDataModel
        imageName = "${data!!.ward}_${data!!.propertyNo}_$userId"

        initViews()

        (supportFragmentManager.findFragmentById(R.id.mapView) as ScrollGoogleMap).getMapAsync(this)
        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!AppUtils.isLocationEnabled(mContext)) {
                // notify user
                val dialog = AlertDialog.Builder(mContext)
                dialog.setMessage("Location not enabled!")
                dialog.setPositiveButton("Open location settings") { paramDialogInterface, paramInt ->
                    val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(myIntent)
                }
                dialog.setNegativeButton("Cancel") { paramDialogInterface, paramInt ->
                    // TODO Auto-generated method stub
                }
                dialog.show()
            }
            buildGoogleApiClient()
        } else {
            Toast.makeText(mContext, "Location not supported in this device", Toast.LENGTH_SHORT).show()
        }

    }

    private fun initViews() {

        //, etCPNumber, etResidentialCount, etCommercialCount,
        //                        etRemarkOne, etRemarkTwo;

        etWard.setText(data!!.ward)
        etZone.setText(data!!.zone)
        etBlock.setText(data!!.block)
        etRegister.setText(data!!.propertyNo)
        etName.setText(data!!.propertyName)
        etAddress.setText(data!!.propertyAddress)

        etCPNumber.setText(data!!.corporationPropertyNumber)
        etResidentialCount.setText(data!!.residentialCount)
        etCommercialCount.setText(data!!.commercialCount)
        etRemarkOne.setText(data!!.remarkOne)
        etRemarkTwo.setText(data!!.remarkTwo)

        tvLat.text = "Latitude\n${String.format("%.8f", data!!.propertyLatitude)}"
        tvLng.text = "Longitude\n${String.format("%.8f", data!!.propertyLongitude)}"

        finalLat = data!!.propertyLatitude!!
        finalLong = data!!.propertyLatitude!!

        if (data!!.propertyImageOne != null && data!!.propertyImageOne != "") {
            ivAddOne.setImageBitmap(createBitmap(data!!.propertyImageOne))
            ivSelectTwo.visibility = View.VISIBLE
        }
        if (data!!.propertyImageTwo != null && data!!.propertyImageTwo != "") {
            ivAddTwo.setImageBitmap(createBitmap(data!!.propertyImageTwo))
            ivSelectThree.visibility = View.VISIBLE
        }
        if (data!!.propertyImageThree != null && data!!.propertyImageThree != "") {
            ivAddThree.setImageBitmap(createBitmap(data!!.propertyImageThree))
            ivSelectFour.visibility = View.VISIBLE
        }
        if (data!!.propertyImageFour != null && data!!.propertyImageFour != "") {
            ivAddFour.setImageBitmap(createBitmap(data!!.propertyImageFour))
        }

        ivSelectOne.setOnClickListener {
            imagePosition = "One"
            imageName = "${etWard.text.toString().trim()}_${etRegister.text.toString().trim()}_${userId}_1"
            showSelectImageDialog()
        }
        ivSelectTwo.setOnClickListener {
            imagePosition = "Two"
            imageName = "${etWard.text.toString().trim()}_${etRegister.text.toString().trim()}_${userId}_2"
            showSelectImageDialog()
        }
        ivSelectThree.setOnClickListener {
            imagePosition = "Three"
            imageName = "${etWard.text.toString().trim()}_${etRegister.text.toString().trim()}_${userId}_3"
            showSelectImageDialog()
        }
        ivSelectFour.setOnClickListener {
            imagePosition = "Four"
            imageName = "${etWard.text.toString().trim()}_${etRegister.text.toString().trim()}_${userId}_4"
            showSelectImageDialog()
        }
        btnUpdate.setOnClickListener { validateData() }

        /*class_spinner.setSelection(classArray.indexOf(cls),true);*/

        val putAdapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, propertyUsageTypeArray)
        spnPUT.adapter = putAdapter
        try {
            spnPUT.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    if (position > 0) {
                        propertyUsageType = propertyUsageTypeArray[position]
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }
        } catch (e: Exception) {

        }

        val ptAdapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, propertyTypeArray)
        spnPT.adapter = ptAdapter
        spnPT.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                if (position > 0) {
                    propertyType = propertyTypeArray[position]
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        val wardAdapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, wardNameArray)
        spnWard.adapter = wardAdapter
        spnWard.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (position > 0) {
                    wardName = wardNameArray[position]
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


        spnPUT.setSelection(propertyUsageTypeArray.indexOf(data!!.propertyUsageType), true)
        spnPT.setSelection(propertyTypeArray.indexOf(data!!.propertyType), true)
        try {
            spnWard.setSelection(wardNameArray.indexOf(data!!.ward), true)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        putAdapter.notifyDataSetChanged()
        ptAdapter.notifyDataSetChanged()

    }

    /***********************************
     * *************************************/


    private fun validateData() {

        var ward = etWard.text.toString().trim()
        var zone = etZone.text.toString().trim()
        var block = etBlock.text.toString().trim()
        var rN = etRegister.text.toString().trim()
        var pN = etName.text.toString().trim()
        var address = etAddress.text.toString().trim()

        val crpnPropertyNumber = etCPNumber.text.toString().trim()
        val rsndtlCount = etResidentialCount.text.toString().trim()
        val cmrclCount = etCommercialCount.text.toString().trim()
        etRemarkOne.text.toString().trim()
        etRemarkTwo.text.toString().trim()

        if (ward != "") {
            data!!.ward = ward
        }
        if (zone != "") {
            data!!.zone = zone
        }
        if (block != "") {
            data!!.block = block
        }
        if (rN != "") {
            data!!.propertyNo = rN
        }
        if (pN != "") {
            data!!.propertyName = pN
        }
        if (address != "") {
            data!!.propertyAddress = address
        }
        if (crpnPropertyNumber != "") {
            data!!.corporationPropertyNumber = crpnPropertyNumber
        }
        if (rsndtlCount != "") {
            data!!.residentialCount = rsndtlCount
        }
        if (cmrclCount != "") {
            data!!.commercialCount = cmrclCount
        }
        if (propertyUsageType != "") {
            data!!.propertyUsageType = propertyUsageType
        }
        if (propertyType != "") {
            data!!.propertyType = propertyType
        }
        if (finalLat!=0.0){
            data!!.propertyLatitude = finalLat
        }

        if (finalLong!=0.0){
            data!!.propertyLongitude = finalLong
        }

        if (imgOneFile != null) {
            data!!.propertyImageOne = imgOneFile!!.absolutePath
        }
        if (imgTwoFile != null) {
            data!!.propertyImageTwo = imgTwoFile!!.absolutePath
        }
        if (imgThreeFile != null) {
            data!!.propertyImageThree = imgThreeFile!!.absolutePath
        }
        if (imgFourFile != null) {
            data!!.propertyImageFour = imgFourFile!!.absolutePath
        }

        updateData()

    }

    private fun updateData() {
        try {
            val updateDao:Dao<CreateDataModel,Int>?=getHelper().getDao(CreateDataModel::class.java)
            val updateBuilder = updateDao!!.updateBuilder()

            updateBuilder.updateColumnValue("ward", data!!.ward)
            updateBuilder.updateColumnValue("zone", data!!.zone)
            updateBuilder.updateColumnValue("block", data!!.block)
            updateBuilder.updateColumnValue("propertyNo", data!!.propertyNo)
            updateBuilder.updateColumnValue("propertyName", data!!.propertyName)
            updateBuilder.updateColumnValue("propertyAddress", data!!.propertyAddress)
            
            updateBuilder.updateColumnValue("corporationPropertyNumber", data!!.corporationPropertyNumber)
            updateBuilder.updateColumnValue("propertyUsageType", data!!.propertyUsageType)
            updateBuilder.updateColumnValue("propertyType", data!!.propertyType)
            updateBuilder.updateColumnValue("residentialCount", data!!.residentialCount)
            updateBuilder.updateColumnValue("commercialCount", data!!.commercialCount)
            updateBuilder.updateColumnValue("remarkOne", data!!.remarkOne)
            updateBuilder.updateColumnValue("remarkTwo", data!!.remarkTwo)

            updateBuilder.updateColumnValue("propertyImageOne", data!!.propertyImageOne)
            updateBuilder.updateColumnValue("propertyImageTwo", data!!.propertyImageTwo)
            updateBuilder.updateColumnValue("propertyImageThree", data!!.propertyImageThree)
            updateBuilder.updateColumnValue("propertyImageFour", data!!.propertyImageFour)
            updateBuilder.updateColumnValue("propertyLatitude", data!!.propertyLatitude)
            updateBuilder.updateColumnValue("propertyLongitude", data!!.propertyLongitude)

            updateBuilder.where().eq("id", data!!.id)

            val isUpdated = updateBuilder.update()
            Log.v("UPDATED_ORM", "update value :$isUpdated")
            if (isUpdated==1){
                showToast("Data Updated Successfully.")
            }else{
                showToast("Some error occur.")
            }

            startActivity(Intent(mContext, MainActivity::class.java))
            finish()
        } catch (e: SQLException) {
            e.printStackTrace()
        }

    }

    private fun getHelper(): DatabaseHelper {
        if (ormLiteDb == null) {
            ormLiteDb = OpenHelperManager.getHelper(this, DatabaseHelper::class.java)
        }
        return ormLiteDb!!
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intentL̥
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            // Create the File where the photo should go
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                ex.printStackTrace()
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(this,
                        "com.kolbrogroup.surveyorapp.fileprovider", photoFile!!)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PICK_IMAGE_ID -> {
                    /*tvMsg.text = "Adjust the student face inside face icon and click save."
                    btnSave.visibility = View.VISIBLE
                    tempImage.visibility = View.VISIBLE
                    txtName.visibility = View.VISIBLE
                    txtName.text = "$i_name"*/
                    try {
                        val bitmap = ImagePicker.getImageFromResult(this, resultCode, data)
                        val path = saveImage(bitmap)
                        when (imagePosition) {
                            "One" -> {
                                ivAddOne.setImageBitmap(bitmap)
                                imgOneFile = path
                                ivSelectTwo.visibility = View.VISIBLE
                            }
                            "Two" -> {
                                ivAddTwo.setImageBitmap(bitmap)
                                imgTwoFile = path
                                ivSelectThree.visibility = View.VISIBLE
                            }
                            "Three" -> {
                                ivAddThree.setImageBitmap(bitmap)
                                imgThreeFile = path
                                ivSelectFour.visibility = View.VISIBLE
                            }
                            "Four" -> {
                                ivAddFour.setImageBitmap(bitmap)
                                imgFourFile = path
                            }
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                    //mImageView.setImageBitmap(bitmap)
                }
                else -> {
                    /*tvMsg.text = "Adjust the student face inside face icon and click save."
                    btnSave.visibility = View.VISIBLE
                    tempImage.visibility = View.VISIBLE
                    txtName.visibility = View.VISIBLE
                    txtName.text = "$i_name"*/
                    try {
                        galleryAddPic()
                        when (imagePosition) {
                            "One" -> {
                                setPic(ivAddOne, imagePosition)
                                ivSelectTwo.visibility = View.VISIBLE
                            }
                            "Two" -> {
                                setPic(ivAddTwo, imagePosition)
                                ivSelectThree.visibility = View.VISIBLE
                            }
                            "Three" -> {
                                setPic(ivAddThree, imagePosition)
                                ivSelectFour.visibility = View.VISIBLE
                            }
                            "Four" -> {
                                setPic(ivAddFour, imagePosition)
                            }
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } else {
            Toast.makeText(mContext, "No Image Selected", Toast.LENGTH_SHORT).show()
        }
    }

    private fun galleryAddPic() {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(mCurrentPhotoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        this.sendBroadcast(mediaScanIntent)
    }

    private fun setPic(mImageView: ImageView, imagePosition: String) {
        // Get the dimensions of the View
        val targetW = mImageView.width
        val targetH = mImageView.height

        // Get the dimensions of the bitmap
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        // Determine how much to scale down the image
        val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true

        val bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)
        //mImageView.scaleType = ImageView.ScaleType.FIT_CENTER
        mImageView.setImageBitmap(bitmap)
        when (imagePosition) {
            "One" -> imgOneFile = saveImage(bitmap)
            "Two" -> imgTwoFile = saveImage(bitmap)
            "Three" -> imgThreeFile = saveImage(bitmap)
            "Four" -> imgFourFile = saveImage(bitmap)
        }
        //width 0.78  and height 1.29
    }

    private fun showSelectImageDialog() {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(mContext)
        dialogBuilder.setTitle("Select Image")
        dialogBuilder.setCancelable(false)

        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_select_image, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()

        val actionCamera = dialogView.findViewById<TextView>(R.id.tvCamera)
        val actionGallery = dialogView.findViewById<TextView>(R.id.tvGallery)
        val actionCancel = dialogView.findViewById<TextView>(R.id.tvCancel)


        actionCancel.setOnClickListener { alertDialog.dismiss() }
        actionCamera.setOnClickListener {
            dispatchTakePictureIntent()
            alertDialog.dismiss()
        }
        actionGallery.setOnClickListener {
            val chooseImageIntent = ImagePicker.getPickImageIntent(this)
            startActivityForResult(chooseImageIntent, PICK_IMAGE_ID)
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val imageFileName = timeStamp
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        /*val image = File.createTempFile(
                imageFileName, *//* prefix *//*
                ".png", *//* suffix *//*
                storageDir      *//* directory *//*
        )*/
        val image = File(storageDir, "$imageFileName.png")

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun saveImage(myBitmap: Bitmap): File {

        val markBitmap = mark(myBitmap, imageName, 30.0f, true)

        val bytes = ByteArrayOutputStream()
        markBitmap.compress(Bitmap.CompressFormat.JPEG, 91, bytes)
        val wallpaperDirectory = File(
                Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY)
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        val f = File(wallpaperDirectory, imageName + Calendar.getInstance()
                .timeInMillis.toString() + ".jpg")
        try {
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                    arrayOf(f.path),
                    arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.absolutePath)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return f
    }


    /**
     *@param a set the alpha component [0..255] of the paint's color.
     * @param textSize set the paint's text size in pixel units.  Set the paint's text size. This value must be > 0
     */
    private fun mark(src: Bitmap, watermark: String, size: Float, underline: Boolean): Bitmap {
        val w = src.getWidth()
        val h = src.getHeight()
        val result = Bitmap.createBitmap(w, h, src.getConfig())

        val resources = mContext.getResources()
        val scale = resources.getDisplayMetrics().density

        val canvas = Canvas(result)
        val s = Rect(0, 0, w, h)
        val d = Rect(0, 0, w, h)
        canvas.drawBitmap(src, s, d, null)

        val paint = Paint()

        paint.setColor(Color.BLACK)
        paint.setAlpha(150)
        canvas.drawRect(0f, h - 40f, w.toFloat(), h.toFloat(), paint)

        val textPaint = TextPaint()
        textPaint.setColor(Color.WHITE)
        textPaint.setAlpha(255)
        //textPaint.setTextSize(size)
        textPaint.setTextSize((14 * scale).toFloat())
        //textPaint.setTextAlign(Paint.Align.CENTER)
        textPaint.setAntiAlias(true)
        textPaint.setUnderlineText(false)

        /*paint.setColor(Color.WHITE)
        paint.setAlpha(255)
        paint.setTextSize(size)
        paint.setAntiAlias(true)
        paint.setUnderlineText(false)*/

        //val xPos = (canvas.getWidth() / 2)
        val xPos = (w - textPaint.getTextSize() * Math.abs(watermark.length / 2)) / 2
        val yPos = ((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2))

        /**
         * Draw the text, with origin at (x,y), using the specified paint. The origin is interpreted
         * based on the Align setting in the paint.
         *
         * @param text The text to be drawn
         * @param x The x-coordinate of the origin of the text being drawn
         * @param y The y-coordinate of the baseline of the text being drawn
         * @param paint The paint used for the text (e.g. color, size, style)
         */
        //canvas.drawText(watermark, 10.0f, h-10f, textPaint)
        //canvas.drawText(watermark, xPos, yPos, textPaint)
        canvas.drawText(watermark, xPos, h - 10f, textPaint)

        return result
    }


    /***********************************
     * *************************************/


    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        /*val mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient)
        if (mLastLocation != null) {
            changeMap(mLastLocation)
            Log.d(TAG, "ON connected")

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        try {
            val mLocationRequest = LocationRequest()
            mLocationRequest.interval = 10000
            mLocationRequest.fastestInterval = 5000
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this)

        } catch (e: Exception) {
            e.printStackTrace()
        }*/

    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i(TAG, "Connection suspended")
        mGoogleApiClient!!.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location?) {
        try {
            if (location != null)
                changeMap(location)
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        mMap!!.setMapType(GoogleMap.MAP_TYPE_SATELLITE)
        mMap!!.getUiSettings().isZoomControlsEnabled = false


        mMap!!.setOnCameraChangeListener { cameraPosition ->
            Log.d("Camera postion change" + "", cameraPosition.toString() + "")
            mCenterLatLong = cameraPosition.target


            mMap!!.clear()

            try {

                val mLocation = Location("")
                mLocation.latitude = mCenterLatLong!!.latitude
                mLocation.longitude = mCenterLatLong!!.longitude

                //startIntentService(mLocation);
                finalLat = mCenterLatLong!!.latitude
                finalLong = mCenterLatLong!!.longitude
                tvLat.text = "Latitude\n${String.format("%.8f", finalLat)}"
                tvLng.text = "Longitude\n${String.format("%.8f", finalLong)}"

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mMap!!.setMyLocationEnabled(false)
        mMap!!.getUiSettings().isMyLocationButtonEnabled = true


        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(data!!.propertyLatitude!!,
            data!!.propertyLongitude!!
        ), 14f))

        (supportFragmentManager.findFragmentById(R.id.mapView) as ScrollGoogleMap).setListener { mScroll.requestDisallowInterceptTouchEvent(true) }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    private fun checkPlayServices(): Boolean {
        val resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show()
            } else {
                //finish();
            }
            return false
        }
        return true
    }

    private fun changeMap(location: Location) {

        Log.d(TAG, "Reaching map$mMap")


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        // check if map is created successfully or not
        /*if (mMap != null) {
            mMap!!.getUiSettings().isZoomControlsEnabled = false
            val latLong: LatLng


            latLong = LatLng(location.latitude, location.longitude)

            val cameraPosition = CameraPosition.Builder()
                    .target(latLong).zoom(19f).tilt(70f).build()

            mMap!!.setMyLocationEnabled(true)
            mMap!!.getUiSettings().isMyLocationButtonEnabled = true
            mMap!!.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition))


            finalLat = location.latitude
            finalLong = location.longitude
            tvLat.text = "Latitude\n${String.format("%.8f",finalLat)}"
            tvLng.text = "Longitude\n${String.format("%.8f",finalLong)}"


        } else {
            Toast.makeText(applicationContext,
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show()
        }*/

    }

    override fun onStart() {
        super.onStart()
        try {
            mGoogleApiClient!!.connect()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onStop() {
        super.onStop()
        try {

        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected()) {
            mGoogleApiClient!!.disconnect()
        }
    }


    private fun createBitmap(path: String?): Bitmap? {
        val bmOptions = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(path, bmOptions)
        try {
            val imageHeight = bmOptions.outHeight
            val imageWidth = bmOptions.outWidth
            bitmap = Bitmap.createScaledBitmap(bitmap, imageWidth, imageHeight, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return bitmap
    }

    fun showHomeBackOnActionbar() {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    fun showHomeBackOnToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
