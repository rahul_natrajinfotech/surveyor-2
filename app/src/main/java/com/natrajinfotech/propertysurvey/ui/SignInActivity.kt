package com.natrajinfotech.propertysurvey.ui



import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.natrajinfotech.propertysurvey.Model.login.LoginModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import com.natrajinfotech.propertysurvey.Utility.BaseActivity
import com.natrajinfotech.propertysurvey.Utility.NetworkHandler
import com.natrajinfotech.propertysurvey.retrofit_provider.RetrofitService.getLoginData
import com.natrajinfotech.propertysurvey.retrofit_provider.WebResponse
import kotlinx.android.synthetic.main.activity_sign_in.*
import retrofit2.Response
import java.util.*


class SignInActivity:BaseActivity(){
    lateinit var permissionsNeeded: ArrayList<String>
    lateinit var permissionsList: ArrayList<String>
    internal val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
lateinit var dialog:Dialog
    private var mContext: Context? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        mContext=this@SignInActivity
        permissionsNeeded = ArrayList()
        permissionsList = ArrayList()
dialog= Dialog((this@SignInActivity))
        permissions()
        inItView()
 }

    private fun inItView() {
        if (AppPreference.getBooleanPreference(mContext, "isLogin")) {
            startActivity(Intent(this@SignInActivity, ContentPageActivity::class.java))
            finish()
        }


        btnLogin.setOnClickListener {
            doSignIn()
        }
        tvForget.setOnClickListener {
            startActivity(Intent(this,ForgetPasswordActivity::class.java))
        }

    }

    fun permissions() {

        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Get Your Location")
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Get Your COARSE Location")
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Take Picture from CAMERA")
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Get your STORAGE Data")

        if (permissionsList.size > 0) {
            if (permissionsNeeded.size > 0) {
                // Need Rationale
                var message = "You need to grant access to " + permissionsNeeded[0]
                for (i in 1 until permissionsNeeded.size)
                    message = message + ", " + permissionsNeeded[i]
                /*showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {*/
                ActivityCompat.requestPermissions(
                    this@SignInActivity, permissionsList.toTypedArray(),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
                )
                /*}
                        });*/
                return
            }
            ActivityCompat.requestPermissions(
                this, permissionsList.toTypedArray(),
                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
            )
            return
        }

        //splash_permission();

    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(this@SignInActivity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission)
            // Check for Rationale Option
            return ActivityCompat.shouldShowRequestPermissionRationale(this@SignInActivity, permission)
        }
        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                // Inisqltial
                perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_COARSE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED


                // Fill with results
                for (i in permissions.indices)
                    perms[permissions[i]] = grantResults[i]
                // Check for ACCESS_FINE_LOCATION
                if (perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.ACCESS_COARSE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                ) {

                    // All Permissions Granted
                    // insertDummyContact();
                    //splash_permission();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                        .show()
                    finish()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun doSignIn() {
        val userName = etUser.text.toString().trim()
        val userPass = etPassword.text.toString().trim()

        if (userName.isEmpty()) {
            showToast("Please enter a valid User Name.")
        } else if (userPass.isEmpty()) {
            showToast("Please enter a valid Password.")
        } else {

            if (NetworkHandler.isNetworkAvailable(mContext)) {
                getLoginData(
                    Dialog(this),
                    retrofitApiClient.loginData(userName, userPass),
                    object : WebResponse {
                        override fun onResponseSuccess(result: Response<*>) {
                            val response = result as Response<LoginModel>
                            val loginModal = response.body()
                            if (loginModal.getStatus()!!) {
                                Toast.makeText(this@SignInActivity, loginModal.getMsg(), Toast.LENGTH_SHORT).show()

                                AppPreference.setBooleanPreference(mContext, "isLogin", true)

                                AppPreference.setStringPreference(
                                    mContext,
                                    "userId",
                                    loginModal.getReport().getUserId()
                                )
                                AppPreference.setStringPreference(
                                    mContext,
                                    "userName",
                                    loginModal.getReport().getUserName()
                                )
                                AppPreference.setStringPreference(
                                    mContext,
                                    "userRole",
                                    loginModal.getReport().getRole()
                                )

                                startActivity(Intent(this@SignInActivity, ContentPageActivity::class.java))
                                finish()
                            } else {
                                Toast.makeText(this@SignInActivity, loginModal.getMsg(), Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onResponseFailed(error: String) {
                            Toast.makeText(this@SignInActivity, error, Toast.LENGTH_SHORT).show()
                        }
                    })
            }



        }
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }


}