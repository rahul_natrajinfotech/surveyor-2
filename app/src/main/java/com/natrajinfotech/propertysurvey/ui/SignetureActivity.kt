package com.natrajinfotech.propertysurvey.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import android.widget.Toast
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.Dao
import com.natrajinfotech.propertysurvey.Database.DatabaseHelper
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.Model.FloorDetailDbModel
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import com.natrajinfotech.propertysurvey.Utility.DigitelSignature
import kotlinx.android.synthetic.main.activity_view.*
import java.io.File



class SignetureActivity:AppCompatActivity(){
    private var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/SurveyorApp/DigitSign/"
    //private var pic_name = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())!!
    private var StoredPath :String=""
    private lateinit var mSigneture:DigitelSignature
    private lateinit var buildingData:BuildingDbModel
    private lateinit var floorList:ArrayList<FloorDetailDbModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.natrajinfotech.propertysurvey.R.layout.activity_view)
inItView()
    }

    private lateinit var file: File

    private fun inItView() {
        buildingData= intent.getSerializableExtra("PROPERTY_DATA") as BuildingDbModel
        floorList= ArrayList()
        floorList= intent.getSerializableExtra("floorList") as ArrayList<FloorDetailDbModel>
        val userId = AppPreference.getStringPreference(this@SignetureActivity, "userId")
        val pic_name="Signeture"+ "${buildingData.wardNumber}_${buildingData.newPropertyNumber}_${userId}_1"
        StoredPath= DIRECTORY + pic_name + ".png"
        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }
        mSigneture=DigitelSignature(this@SignetureActivity,null,lLinear)
        mSigneture.setBackgroundColor(Color.WHITE)
        lLinear.addView(mSigneture, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        btnNext.setOnClickListener {
            @Suppress("DEPRECATION")
            lLinear!!.isDrawingCacheEnabled = true
            if (mSigneture.save(lLinear,StoredPath)){
buildingData.signetureImage=StoredPath
                saveIntoDb()

            }else{
                Toast.makeText(this@SignetureActivity,"There is issue in saving signeture.",Toast.LENGTH_LONG).show()

            }
        }


    }

    private fun saveIntoDb() {
try {


    val dataHelper = OpenHelperManager.getHelper(this@SignetureActivity, DatabaseHelper::class.java)
    val buildingDao: Dao<BuildingDbModel, Int>? = dataHelper.getDao(BuildingDbModel::class.java)
    buildingDao!!.create(buildingData)
    Log.v("dip","image url :"+buildingData.propertyImageOne)
    val floorDao: Dao<FloorDetailDbModel, Int>? = dataHelper.getDao(FloorDetailDbModel::class.java)
    if (floorList.size != 0) {
        for (i in floorList.indices) {
            floorDao!!.create(floorList[i])
        }
    }
Toast.makeText(this@SignetureActivity,"Data inserted successfully.",Toast.LENGTH_LONG).show()
    startActivity(Intent(this@SignetureActivity,ContentPageActivity::class.java))
    finish()
}catch (e:Exception){
    Log.v("dip","database inserting error :"+e.message)

}
    }
}