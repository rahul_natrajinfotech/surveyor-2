package com.natrajinfotech.propertysurvey.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.Model.FloorDetailDbModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.adapter.RvFlorrDetailAdaptor
import kotlinx.android.synthetic.main.activity_add_floor_main.*

class AddFloorMainActivity:AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_floor_main)
        inItView()
 }

    private fun inItView() {
        //getting data from previous activity.
        val buildingData: BuildingDbModel?= intent.getSerializableExtra("PROPERTY_DATA") as BuildingDbModel?
        val floorList:ArrayList<FloorDetailDbModel>?= intent.getSerializableExtra("floorList") as ArrayList<FloorDetailDbModel>?
//setting data on recyclerview.
        rvFlorDetail.layoutManager=LinearLayoutManager(this@AddFloorMainActivity)
        rvFlorDetail.adapter=RvFlorrDetailAdaptor(this@AddFloorMainActivity, floorList!!)

        btnAddFloor.setOnClickListener {

            startActivity(
                Intent(this,AddFloorActivity::class.java)
                    .putExtra("floorList",floorList)
                    .putExtra("PROPERTY_DATA",buildingData)
                    .putExtra("from","add"))
        }

        btnSaveForm.setOnClickListener {
            startActivity(
                Intent(this,SelectPictureActivity::class.java)
                    .putExtra("PROPERTY_DATA",buildingData)
                    .putExtra("floorList",floorList)
                    .putExtra("from","floor"))
            finish()
        }

    }
}