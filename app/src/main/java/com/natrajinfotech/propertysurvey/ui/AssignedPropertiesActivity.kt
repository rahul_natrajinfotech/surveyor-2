package com.natrajinfotech.propertysurvey.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.table.TableUtils
import com.natrajinfotech.propertysurvey.Database.DatabaseHelper
import com.natrajinfotech.propertysurvey.Model.AssignPropertyDbModel
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.Model.GetAssignPropertyListResponseModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import com.natrajinfotech.propertysurvey.Utility.BaseActivity
import com.natrajinfotech.propertysurvey.Utility.NetworkHandler
import com.natrajinfotech.propertysurvey.adapter.RvAssignPropertyAdaptor
import com.natrajinfotech.propertysurvey.retrofit_provider.RetrofitService
import com.natrajinfotech.propertysurvey.retrofit_provider.WebResponse
import kotlinx.android.synthetic.main.activity_assigned_properties.*
import retrofit2.Response

class AssignedPropertiesActivity:BaseActivity(),View.OnClickListener{
    lateinit var assignPropertyList:ArrayList<AssignPropertyDbModel>
    lateinit var assignDao:Dao<AssignPropertyDbModel,Int>
    lateinit var dataBaseHelper:DatabaseHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assigned_properties)


        assignPropertyList= ArrayList()
        inItView()



    }

    private fun inItView() {
        dataBaseHelper=OpenHelperManager.getHelper(this@AssignedPropertiesActivity,DatabaseHelper::class.java)
        assignDao=dataBaseHelper.getDao(AssignPropertyDbModel::class.java)
        val assignList=assignDao.queryForAll()
        if (assignList.size==0){
            getAssignPropertyList()

        }else{
            rvAssignProperty.layoutManager=LinearLayoutManager(this@AssignedPropertiesActivity)
            assignPropertyList= assignList as ArrayList<AssignPropertyDbModel>
            rvAssignProperty.adapter=RvAssignPropertyAdaptor(this,assignPropertyList,this@AssignedPropertiesActivity)

        }
    btnRefresh.setOnClickListener(this)
    }

    private fun getAssignPropertyList() {
        if (NetworkHandler.isNetworkAvailable(this@AssignedPropertiesActivity)){
            val userId = AppPreference.getStringPreference(this@AssignedPropertiesActivity, "userId")
            RetrofitService.getAssignPropertyList(Dialog(this@AssignedPropertiesActivity),retrofitApiClient.getAssignPropertyList(userId),object :
                WebResponse {
                override fun onResponseSuccess(result: Response<*>?) {
                    val response = result as Response<GetAssignPropertyListResponseModel>
                    val assignModel=response.body()
                    if (assignModel.status){
assignPropertyList= ArrayList()
                        assignPropertyList= assignModel.Report!!
rvAssignProperty.layoutManager=LinearLayoutManager(this@AssignedPropertiesActivity)
rvAssignProperty.adapter=RvAssignPropertyAdaptor(this@AssignedPropertiesActivity,assignPropertyList,this@AssignedPropertiesActivity)
TableUtils.clearTable(dataBaseHelper.connectionSource,AssignPropertyDbModel::class.java)
                        for (i in assignPropertyList.indices){
                            assignDao.create(assignPropertyList[i])

                        }
                    }else{
                        Toast.makeText(this@AssignedPropertiesActivity,assignModel.msg, Toast.LENGTH_LONG).show()

                    }
                }

                override fun onResponseFailed(error: String?) {

                }


            })

        }else{
            Toast.makeText(this@AssignedPropertiesActivity,"Sorry! You are not connected with internet please check it & try again.",
                Toast.LENGTH_LONG).show()

        }
    }override fun onClick(p0: View?) {
when (p0?.id) {
    R.id.tvView -> {
        val selectedPosition:Int= p0.tag as Int
        showNextDetail(selectedPosition)

    }
    R.id.tvAction -> {
        val selectedPosition:Int= p0.tag as Int

        startActivity(Intent(this@AssignedPropertiesActivity,PropertyTypeActivity::class.java)
    .putExtra("propertyData",assignPropertyList[selectedPosition]))
        finish()
    }
    R.id.llAssignMain -> {
        val selectedPosition:Int= p0.tag as Int

        startActivity(Intent(this@AssignedPropertiesActivity,PropertyTypeActivity::class.java)
            .putExtra("propertyData",assignPropertyList[selectedPosition]))
        finish()

    }
    R.id.btnRefresh -> {
        getAssignPropertyList()

    }

}
    }

    private fun showNextDetail(selectedPosition: Int) {
        val buildingDao:Dao<BuildingDbModel,Int>?=dataBaseHelper.getDao(BuildingDbModel::class.java)
        val buildingList:ArrayList<BuildingDbModel>?= buildingDao!!.queryForEq("PropertyId",assignPropertyList[selectedPosition].PropertyId) as ArrayList<BuildingDbModel>?
        if (buildingList.isNullOrEmpty()){
        startActivity(Intent(this@AssignedPropertiesActivity,PropertyTypeActivity::class.java)
            .putExtra("propertyData",assignPropertyList[selectedPosition]))
        finish()
        }else{
            if (buildingList[0].synced){
                Toast.makeText(this@AssignedPropertiesActivity,"You can not view property detail which already sync on server.",Toast.LENGTH_LONG).show()

            }else{
                startActivity(Intent(this@AssignedPropertiesActivity,PropertyTypeActivity::class.java)
                    .putExtra("propertyData",assignPropertyList[selectedPosition]))
                finish()

            }

        }
    }


}