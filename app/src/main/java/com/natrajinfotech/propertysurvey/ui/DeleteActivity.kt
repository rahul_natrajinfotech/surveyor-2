package com.natrajinfotech.propertysurvey.ui







import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.Toast
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.Dao
import com.kolbrogroup.surveyorapp.utils.AppAlerts
import com.natrajinfotech.propertysurvey.Database.DatabaseHelper
import com.natrajinfotech.propertysurvey.MainActivity
import com.natrajinfotech.propertysurvey.Model.CreateDataModel
import com.natrajinfotech.propertysurvey.Model.login.LoginModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import com.natrajinfotech.propertysurvey.Utility.BaseActivity
import com.natrajinfotech.propertysurvey.Utility.NetworkHandler
import com.natrajinfotech.propertysurvey.retrofit_provider.RetrofitService
import com.natrajinfotech.propertysurvey.retrofit_provider.WebResponse
import kotlinx.android.synthetic.main.activity_delete.*
import retrofit2.Response
import java.sql.SQLException
import java.util.*


class DeleteActivity : BaseActivity() {

    private lateinit var mContext : Context
    private var userId = ""
    private var dataList : List<CreateDataModel> = ArrayList()

    // Reference of DatabaseHelper class to access its DAOs and other components
    private var ormLiteDb :DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete)

        mContext = this
        this.title = "Delete"

        showHomeBackOnActionbar()

        initViews()
    }

    private fun initViews() {
        userId = AppPreference.getStringPreference(mContext, "userId")
        ivNext.setOnClickListener { doSignIn() }
    }

    private fun doSignIn() {
        val userName = etUser.text.toString().trim()
        val userPass = etPassword.text.toString().trim()

        if (userName.isEmpty()) {
            showToast("User name must not be empty.")
        } else if (userPass.isEmpty()) {
            showToast("Password must not be empty.")
        } else {

            if (NetworkHandler.isNetworkAvailable(mContext)) {
                RetrofitService.getLoginData(Dialog(this), retrofitApiClient.loginData(userName, userPass), object :
                    WebResponse {
                    override fun onResponseSuccess(result: Response<*>) {
                        val response = result as Response<LoginModel>
                        val loginModal = response.body()
                        if (loginModal.status!!) {
                            if (userId==loginModal.report.userId) {
                                performDelete()
                                /*startActivity(Intent(mContext, NumberingActivity::class.java))
                                finish()*/
                            }else{
                                AppAlerts.showAlertMessage(mContext,"Authentication Failed!", "You must login with same user which you provides.")
                            }
                        } else {
                            AppAlerts.showAlertMessage(mContext,"Authentication Failed!", loginModal.msg)
                        }
                    }

                    override fun onResponseFailed(error: String) {
                        showToast(error)
                    }
                })
            }

        }
    }

    private fun performDelete() {
        try {
            val propertyDao = getHelper().getDao(CreateDataModel::class.java)
            dataList = propertyDao.queryForAll()
            //Toast.makeText(this, ""+dataList.size(), Toast.LENGTH_SHORT).show();
                var i=0
            var count = 0
            for(dataModel in dataList){
                i++
                if (dataModel.synced!!){
                    val deleteDao:Dao<CreateDataModel,Int>?=getHelper().getDao(CreateDataModel::class.java)
                    deleteDao!!.deleteById(dataModel.id)
                    count++
                }
                if (i==dataList.size){
                    if (count>0) {
                        showToast("$count data deleted successfully.")
                    } else {
                        showToast("Their is no deletable data.")
                    }

                    val intent = Intent(mContext, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                }
            }
        } catch (e: SQLException) {
            e.printStackTrace()
        }

    }

    private fun getHelper():DatabaseHelper  {
        if (ormLiteDb == null) {
            ormLiteDb = OpenHelperManager.getHelper(this, DatabaseHelper::class.java)
        }
        return ormLiteDb!!
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun showHomeBackOnActionbar() {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    fun showHomeBackOnToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
