package com.natrajinfotech.propertysurvey.ui

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.natrajinfotech.propertysurvey.Model.CreateDataModel
import com.natrajinfotech.propertysurvey.R
import com.natrajinfotech.propertysurvey.Utility.AppPreference
import kotlinx.android.synthetic.main.activity_property_detail.*

class PropertyDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mContext : Context
    private var data : CreateDataModel? = null
    private var mMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_detail)

        mContext = this
        this.title = "Details"
        showHomeBackOnActionbar()

        data = intent.getSerializableExtra("DATA_MODEL") as CreateDataModel

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.mapView) as SupportMapFragment

        mapFragment.getMapAsync(this)

        initViews()
    }

    /*var bundle = Bundle()
        bundle.putParcelable("selected_person",person)
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(data!!.propertyLatitude, data!!.propertyLongitude), 14f))*/

    private fun initViews() {
        tvWard.text = data!!.ward
        tvZone.text = data!!.zone
        tvBlock.text = data!!.block
        tvPropertyNo.text = data!!.propertyNo
        tvPropertyName.text = data!!.propertyName
        tvAddress.text = data!!.propertyAddress

        tvCPN.text = data!!.corporationPropertyNumber
        tvPUT.text = data!!.propertyUsageType
        tvPT.text = data!!.propertyType
        tvResidentialCount.text = data!!.residentialCount
        tvCommercialCount.text = data!!.commercialCount
        tvRemarkOne.text = data!!.remarkOne
        tvRemarkTwo.text = data!!.remarkTwo

        tvLat.text = data!!.propertyLatitude.toString()
        tvLng.text = data!!.propertyLongitude.toString()
        if (data!!.propertyImageOne!=null && data!!.propertyImageOne!="") {
            iv1.setImageBitmap(createBitmap(data!!.propertyImageOne))
        }
        if (data!!.propertyImageTwo!=null && data!!.propertyImageTwo!="") {
            iv2.setImageBitmap(createBitmap(data!!.propertyImageTwo))
        }
        if (data!!.propertyImageThree!=null && data!!.propertyImageThree!="") {
            iv3.setImageBitmap(createBitmap(data!!.propertyImageThree))
        }
        if (data!!.propertyImageFour!=null && data!!.propertyImageFour!="") {
            iv4.setImageBitmap(createBitmap(data!!.propertyImageFour))
        }
    }

    private fun createBitmap(path: String?): Bitmap? {
        val bmOptions = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(path,bmOptions)
        try {
            val imageHeight = bmOptions.outHeight
            val imageWidth = bmOptions.outWidth
            bitmap = Bitmap.createScaledBitmap(bitmap,imageWidth,imageHeight,true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return bitmap
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (AppPreference.getStringPreference(mContext, "userRole") == "admin"  && !data!!.synced!!){
            menuInflater.inflate(R.menu.menu_edit, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == R.id.action_edit){
            startActivity(Intent(mContext, EditPropertyDetailActivity::class.java).putExtra("DATA_MODEL", data))
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        mMap!!.setMapType(GoogleMap.MAP_TYPE_SATELLITE)
        mMap!!.getUiSettings().isZoomControlsEnabled = false

        val latLng = LatLng(data!!.propertyLatitude!!, data!!.propertyLongitude!!)

        val icon = BitmapDescriptorFactory.fromResource(R.drawable.blue_32_pin)

        val markerOptions = MarkerOptions().position(latLng).icon(icon)

        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
        mMap!!.addMarker(markerOptions)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun showHomeBackOnActionbar() {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    /*BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);
        bitmap = Bitmap.createScaledBitmap(bitmap,parent.getWidth(),parent.getHeight(),true);
        imageView.setImageBitmap(bitmap);*/
}
