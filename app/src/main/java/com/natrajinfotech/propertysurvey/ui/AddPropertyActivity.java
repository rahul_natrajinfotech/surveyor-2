package com.natrajinfotech.propertysurvey.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.*;
import com.natrajinfotech.propertysurvey.Model.CreateDataModel;
import com.natrajinfotech.propertysurvey.R;
import com.natrajinfotech.propertysurvey.Utility.AppPreference;


public class AddPropertyActivity extends AppCompatActivity {

    private EditText etWard, etZone, etBlock, etRegister, etName, etAddress, etCPNumber, etResidentialCount, etCommercialCount,
                        etRemarkOne, etRemarkTwo;
    private Spinner spnPUT, spnPT, spnWard;
    private CreateDataModel propertyModel;

    private String[] propertyUsageTypeArray = {"Select a Property Usage Type", "RESIDENTIAL", "COMMERCIAL", "MIXED", "INDUSTRIAL"};
    private String[] wardNameArray = {"Select a Ward", "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T"};
    private String[] propertyTypeArray = {"Select a Property Type", "APARTMENT", "CHAWL", "SLUM", "BUNGALOW", "INDIVIDUAL HOUSE",
                                            "ROW HOUSE", "SHOP", "OFFICE", "OTHERS", "MIXED", "INDUSTRIAL"};

    private String propertyUsageType = "";
    private String propertyType = "";
    private String wardName = "";

    private Context mContext;

    /*private val startUpStringArray = arrayOf("Select start row","Dear Parents","Dear Student")
    private val endStringArray = arrayOf("Select end row","From St. Anthonys School", "From Principal", "From Office",
            "From Class Teacher", "From Sports Teacher")
    private var startUpString = ""
    private var endString = ""*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_property);
        mContext = this;

        this.setTitle("Property Data");

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String preSelectWard = AppPreference.getStringPreference(mContext, "WARD");
        String preSelectZone = AppPreference.getStringPreference(mContext, "ZONE");

        propertyModel = new CreateDataModel();

        etWard = findViewById(R.id.etWard);
        etZone = findViewById(R.id.etZone);
        etBlock = findViewById(R.id.etBlock);
        etRegister = findViewById(R.id.etRegister);
        etName = findViewById(R.id.etName);
        etAddress = findViewById(R.id.etAddress);

        etCPNumber = findViewById(R.id.etCPNumber);
        etResidentialCount = findViewById(R.id.etResidentialCount);
        etCommercialCount = findViewById(R.id.etCommercialCount);
        etRemarkOne = findViewById(R.id.etRemarkOne);
        etRemarkTwo = findViewById(R.id.etRemarkTwo);

        spnPUT = findViewById(R.id.spnPUT);
        spnPT = findViewById(R.id.spnPT);
        spnWard = findViewById(R.id.spnWard);

        ArrayAdapter putAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, propertyUsageTypeArray);
        spnPUT.setAdapter(putAdapter);
        spnPUT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0) {
                    propertyUsageType = propertyUsageTypeArray[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        putAdapter.notifyDataSetChanged();

        ArrayAdapter ptAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, propertyTypeArray);
        spnPT.setAdapter(ptAdapter);
        spnPT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0) {
                    propertyType = propertyTypeArray[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ptAdapter.notifyDataSetChanged();

        ArrayAdapter wardAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, wardNameArray);
        spnWard.setAdapter(wardAdapter);
        spnWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0) {
                    wardName = wardNameArray[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!preSelectWard.isEmpty()){
            //spnWard.setSelection(wardNameArray.indexOf(preSelectWard), true);
            int i = 0;
            for (String s:wardNameArray) {
                if (s.equals(preSelectWard)){
                    spnWard.setSelection(i, true);
                    etZone.setText(preSelectZone);
                }
                i++;
            }
        }
        wardAdapter.notifyDataSetChanged();



        ((Button)findViewById(R.id.btnSaveProperty)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndNext();
            }
        });
    }

    private void validateAndNext() {
        String ward, zone, block, registerNumber, name, address, corporationPropertyNumber, residentialCount,
                commercialCount, remark1, remark2;
        //ward = etWard.getText().toString().trim();
        zone = etZone.getText().toString().trim();
        block = etBlock.getText().toString().trim();
        registerNumber = etRegister.getText().toString().trim();
        name = etName.getText().toString().trim();
        address = etAddress.getText().toString().trim();

        corporationPropertyNumber = etCPNumber.getText().toString().trim();
        residentialCount = etResidentialCount.getText().toString().trim();
        commercialCount = etCommercialCount.getText().toString().trim();
        remark1 = etRemarkOne.getText().toString().trim();
        remark2 = etRemarkTwo.getText().toString().trim();



        if (wardName.isEmpty()){
            showToast("Select a Ward!");
        }else if (zone.isEmpty()){
            showToast("Input Field Zone must not be empty!");
        }else if (block.isEmpty()){
            showToast("Input Field Block must not be empty!");
        }else if (registerNumber.isEmpty()){
            showToast("Input Field Register number must not be empty!");
        }else if (name.isEmpty()){
            showToast("Input Field Name must not be empty!");
        }else if (address.isEmpty()){
            showToast("Input Field Address must not be empty!");
        }else if (corporationPropertyNumber.isEmpty()){
            showToast("Input Field Corporation property number  must not be empty!");
        }else if (residentialCount.isEmpty()){
            showToast("Input Field Residential count must not be empty!");
        }else if (commercialCount.isEmpty()){
            showToast("Input Field Commercial count  must not be empty!");
        }else if (propertyUsageType.isEmpty()){
            showToast("Select a Property usage type!");
        }else if (propertyType.isEmpty()){
            showToast("Select a Property type!");
        }else{
            propertyModel.setWard(wardName);
            propertyModel.setZone(zone);
            propertyModel.setBlock(block);
            propertyModel.setPropertyNo(registerNumber);
            propertyModel.setPropertyName(name);
            propertyModel.setPropertyAddress(address);

            propertyModel.setCorporationPropertyNumber(corporationPropertyNumber);
            propertyModel.setResidentialCount(residentialCount);
            propertyModel.setCommercialCount(commercialCount);
            propertyModel.setPropertyUsageType(propertyUsageType);
            propertyModel.setPropertyType(propertyType);
            propertyModel.setRemarkOne(remark1);
            propertyModel.setRemarkTwo(remark2);

            AppPreference.setStringPreference(mContext, "WARD", wardName);
            AppPreference.setStringPreference(mContext, "ZONE", zone);

            Intent in=new Intent(AddPropertyActivity.this,SelectPropertyPictureActivity.class);
            in.putExtra("PROPERTY_DATA", propertyModel);
            startActivity(in);
        }
    }

    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
