package com.natrajinfotech.propertysurvey.ui

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.natrajinfotech.propertysurvey.Model.AssignPropertyDbModel
import com.natrajinfotech.propertysurvey.R
import kotlinx.android.synthetic.main.activity_property_type.*

class PropertyTypeActivity:AppCompatActivity(), View.OnClickListener, OnMapReadyCallback,
GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener,
com.google.android.gms.location.LocationListener{


    private val  propertyArray  = arrayOf(
        "Apartment",
        "Chawl",
        "Commercial complex",
        "Individual House",
        "Industry",
        "Open Land",
        "Shop lane",
        "Shopping mall",
        "Slum"
    )
    private var propertyType = ""
    var lat:Double=0.0
    var lng:Double=0.0
    lateinit var mMapView: MapView
    lateinit var mGoogleMap: GoogleMap
    lateinit var mGoogleApiClient: GoogleApiClient
    private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
lateinit var propertyData:AssignPropertyDbModel
    var propertyPosition:Int=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_type)
fragmentMap.onCreate(savedInstanceState)
inItView()
        btnSaveGeo.setOnClickListener {
            propertyData.Latitude=lat.toString()
            propertyData.Longitude=lng.toString()
            startActivity(Intent(this,SurveyStatusActivity::class.java)
                .putExtra("propertyData",propertyData))
            finish()
        }
    }

    private fun inItView() {
propertyData= intent.getSerializableExtra("propertyData") as AssignPropertyDbModel
        lat=propertyData.Latitude.toDouble()
        lng=propertyData.Longitude.toDouble()
        tvLatitude.text=lat.toString()
        tvLongitude.text=lng.toString()
        setSpinnerData()
        setMap()
    }

    private fun setSpinnerData() {
for (i in propertyArray.indices){
    if (propertyArray[i].equals(propertyData.PropertyType)){
        propertyPosition=i+1
    }
}
        val putAdapter = ArrayAdapter(this, R.layout.spinner_list, propertyArray)
        spinnerProperty?.adapter=  putAdapter
spinnerProperty.setSelection(propertyPosition)
        spinnerProperty?.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position>0){
                    propertyType = propertyArray[position]
                }
            }

        }
        putAdapter.notifyDataSetChanged()


    }

    private fun setMap() {
        try {

            fragmentMap.onResume()
            MapsInitializer.initialize(this@PropertyTypeActivity)

            fragmentMap.getMapAsync(object : OnMapReadyCallback {
                override fun onMapReady(p0: GoogleMap?) {
                    mGoogleMap= p0!!
                    mGoogleMap.isMyLocationEnabled=true
                    mGoogleMap.uiSettings.isMyLocationButtonEnabled=true

                    var marker= MarkerOptions().position(LatLng(lat,lng))
                        .title("Your Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                    //mGoogleMap.addMarker(marker)
                    var cameraPosition= CameraPosition.Builder().target(LatLng(lat,lng)).zoom(15F).build()
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                    mGoogleMap.setOnCameraChangeListener(object :GoogleMap.OnCameraChangeListener{
                        override fun onCameraChange(p0: CameraPosition?) {
                            lat= p0!!!!.target.latitude
                            lng=p0.target.longitude
                            tvLatitude.text=lat.toString()
                            tvLongitude.text=lng.toString()


                        }

                    })
                }


            })
        }catch (e:Exception){
            Log.v("dip","map error :"+e.message)

        }
    }
    override fun onMapReady(p0: GoogleMap?) {

    }

    override fun onConnected(p0: Bundle?) {
//var mLocation=LocationServices.FusedLocationApi.getLastLocation(mgoa)
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(p0: Location?) {

    }

    override fun onClick(p0: View?) {

    }
}