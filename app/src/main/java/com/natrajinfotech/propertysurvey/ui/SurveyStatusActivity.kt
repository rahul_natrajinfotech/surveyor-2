package com.natrajinfotech.propertysurvey.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.Dao
import com.natrajinfotech.propertysurvey.Database.DatabaseHelper
import com.natrajinfotech.propertysurvey.Model.AssignPropertyDbModel
import com.natrajinfotech.propertysurvey.Model.BuildingDbModel
import com.natrajinfotech.propertysurvey.R
import kotlinx.android.synthetic.main.activity_survey_status.*

class SurveyStatusActivity:AppCompatActivity(){

    private val  surveyStatusArray  = arrayOf(
        "Denied",
        "Door Lock",
        "Pending"
    )

    private var surveyStatus = ""
    private lateinit var propertyData:AssignPropertyDbModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey_status)


        inItView()
   }

    private fun inItView() {
        propertyData= intent.getSerializableExtra("propertyData") as AssignPropertyDbModel

        setSpinnerData()
        clickLisner()
    }

    private fun clickLisner() {

        btnSave.setOnClickListener {
            saveSurveyStatus()

        }
        btnFillForm.    setOnClickListener {
            startActivity(Intent(this@SurveyStatusActivity,SurveyFormActivity::class.java)
                .putExtra("propertyData",propertyData))
            finish()

        }

        //Button copy from previous code here

    }

    private fun saveSurveyStatus() {
        val dataBaseHelper:DatabaseHelper=OpenHelperManager.getHelper(this@SurveyStatusActivity,DatabaseHelper::class.java)
        val insertData:Dao<BuildingDbModel,Int>?=dataBaseHelper.getDao(BuildingDbModel::class.java)
        val insertModel=BuildingDbModel()
        insertModel.propertyId=propertyData.PropertyId
        insertModel.propertyType=propertyData.PropertyType
        insertModel.wardNumber=propertyData.WardName
        insertModel.zoneNumber=propertyData.ZoneName
        insertModel.blockNumber=propertyData.BlockNumber
        insertModel.electionWardNumber=""
        insertModel.villageName=""
        insertModel.plotNumber=""
        insertModel.societyName=""
        insertModel.buildingName=""
insertModel.existingPropertyNumber=""
        insertModel.newPropertyNumber=""
        insertModel.surveyNumber=""
        insertModel.address=propertyData.Address
        insertModel.pinCode=""
        insertModel.landMark=""
        insertModel.ownerFirstName=""
        insertModel.ownerMiddleName=""
        insertModel.ownerLastName=""
        insertModel.categoryOfOwner=""
        insertModel.aadharNoOfOwner=""
        insertModel.ownerMobile=""
        insertModel.ownerEmail=""
        insertModel.occupeirFirstName=""
        insertModel.occupeirMiddleName=""
        insertModel.occupeirLastName=""
        insertModel.occupeirMobile=""
        insertModel.occupeirAadharNumber=""
        insertModel.tenantFirstName=""
        insertModel.tenantMiddleName=""
        insertModel.tenantLastName=""
        insertModel.tenantMobile=""
        insertModel.tenantAadharNumber=""
        insertModel.monthlyRent=""
        insertModel.establishmentName=""
        insertModel.isGarden=""
        insertModel.isPool=""
        insertModel.isClub=""
        insertModel.usesType=propertyData.PropertyUsageType
        insertModel.constructionType=""
        insertModel.totalPlotArea=""
        insertModel.latitude=propertyData.Latitude
        insertModel.longitude=propertyData.Longitude
        insertModel.majorRoadName=""
        insertModel.majorRoadWidth=""
        insertModel.minorRoadName=""
        insertModel.minorRoadWidth=""
        insertModel.assessmentYear=""
        insertModel.buildingAge=""
        insertModel.oldAssessmentValue=""
        insertModel.yearOfBuildinConstruction=""
        insertModel.buildingPermissionYear=""
        insertModel.legalStatus=""
        insertModel.buildingPermissionNo=""
        insertModel.isStp=""
        insertModel.isRainWaterSystem=""
        insertModel.isSolarSystem=""
        insertModel.numberOfToilet=""
        insertModel.waterConnection=""
        insertModel.pipeSize=""
        insertModel.isWaterMeter=""
        insertModel.isElectricityConnection=""
        insertModel.isLift=""
        insertModel.isSepticTenk=""
        insertModel.septicTenkDimension=""
        insertModel.isAdvertisementSpace=""
        insertModel.typeOfAdvertisment=""
        insertModel.SurveyStatus=surveyStatus
        insertModel.propertyImageOne=""
        insertModel.propertyImageTwo=""
        insertModel.propertyImageThree=""
        insertModel.propertyImageFour
        insertModel.signetureImage=""
        insertModel.remark=etRemark.text.toString().trim()
        insertData!!.create(insertModel)


    }

    private fun setSpinnerData() {
        val putAdapter = ArrayAdapter(this, R.layout.spinner_list, surveyStatusArray)
        spinnerSurvey?.adapter=  putAdapter
        spinnerSurvey?.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position>0){
                    surveyStatus = surveyStatusArray[position]
                }
            }

        }
        putAdapter.notifyDataSetChanged()
    }

}
