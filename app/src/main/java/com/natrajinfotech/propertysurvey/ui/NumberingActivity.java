package com.natrajinfotech.propertysurvey.ui;

import android.app.Dialog;
import android.content.*;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.natrajinfotech.propertysurvey.Database.DatabaseHelper;
import com.natrajinfotech.propertysurvey.Model.CreateDataModel;
import com.natrajinfotech.propertysurvey.R;
import com.natrajinfotech.propertysurvey.Utility.AppPreference;
import com.natrajinfotech.propertysurvey.Utility.AppProgressDialog;
import com.natrajinfotech.propertysurvey.Utility.BaseActivity;
import com.natrajinfotech.propertysurvey.Utility.NetworkHandler;
import com.natrajinfotech.propertysurvey.adapter.PropertyListAdapter;
import com.natrajinfotech.propertysurvey.firebase.Constants;
import com.natrajinfotech.propertysurvey.retrofit_provider.RetrofitService;
import com.natrajinfotech.propertysurvey.retrofit_provider.WebResponse;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class NumberingActivity extends BaseActivity implements View.OnClickListener{

    //firebase objects
    private StorageReference storageReference;

    private String imageUrl = "";

    private String imgWeb1 = "";
    private String imgWeb2 = "";
    private String imgWeb3 = "";
    private String imgWeb4 = "";


    private Context mContext;
    private TextView mTextMessage;
    private List<CreateDataModel> dataList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PropertyListAdapter adapter;
    // Reference of DatabaseHelper class to access its DAOs and other components
    private DatabaseHelper ormLiteDb = null;
    private Dialog mDialog;

    private File myTempFile;
    private String userId;

//DATA_MODEL
    Button btnSetting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbering);
        mContext = this;
        //Fabric.with(this, new Crashlytics());
        userId = AppPreference.getStringPreference(mContext, "userId");

        mDialog = new Dialog(mContext);

        storageReference = FirebaseStorage.getInstance().getReference();

        initPropertyData();

        recyclerView = (RecyclerView)findViewById(R.id.rvProperty);

        ((Button)findViewById(R.id.btnPhoto)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NumberingActivity.this, AddPropertyActivity.class));

            }
        });

        ((Button)findViewById(R.id.btnSync)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSync();
            }
        });

        if (dataList.size()>0) {
            ((TextView)findViewById(R.id.tvError)).setVisibility(View.GONE);
            ((RelativeLayout)findViewById(R.id.linearLayout2)).setVisibility(View.VISIBLE);
            prepareRecyclerView();
        }else{
            ((TextView)findViewById(R.id.tvError)).setVisibility(View.VISIBLE);
            ((RelativeLayout)findViewById(R.id.linearLayout2)).setVisibility(View.GONE);
        }

        creteTempFile();

    }

    private void performSync() {
        if (dataList.size()>0){
            AppProgressDialog.show(mDialog);
             prepareDataForServer(0);
        }
    }

    private void prepareDataForServer(int p){
        CreateDataModel data = dataList.get(p);
        p++;
        boolean isDone = false;
        if (!data.getSynced()){
            if (dataList.size()==p){
                isDone = true;
            }
            sendOnServer(data, isDone, p);
        }else{
            if (dataList.size()>p){
                prepareDataForServer(p);
            }else{
                AppProgressDialog.hide(mDialog);
                Toast.makeText(mContext, "Sync Completed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void prepareRecyclerView() {
        adapter = new PropertyListAdapter(mContext, (ArrayList<CreateDataModel>) dataList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void initPropertyData() {
        try {
            Dao<CreateDataModel, Integer> propertyDao = getHelper().getDao(CreateDataModel.class);
            dataList = propertyDao.queryForAll();
            //Toast.makeText(this, ""+dataList.size(), Toast.LENGTH_SHORT).show();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DatabaseHelper getHelper() {
        if (ormLiteDb == null){
            ormLiteDb = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return ormLiteDb;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (!AppPreference.getStringPreference(mContext, "userRole").equals("admin")) {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_add_new:
                startActivity(new Intent(NumberingActivity.this, AddPropertyActivity.class));
                break;
            case R.id.action_logout:
                logout();
                break;
            case R.id.action_delete:
                startActivity(new Intent(mContext, DeleteActivity.class));
                break;
        }
        /*if (item.getItemId()==R.id.action_add_new){
            startActivity(new Intent(NumberingActivity.this, AddPropertyActivity.class));
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Are you sure want to Logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                })
                .setNegativeButton("No", null)
                .create()
                .show();
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(this);
        Intent intent = new Intent(this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private void creteTempFile() {
        Bitmap myLogo = ((BitmapDrawable) ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.img_placeholder, null)).getBitmap();
                myTempFile = bitmapToFile(myLogo);
    }

    // Method to save an bitmap to a file
    private File bitmapToFile(Bitmap bitmap) {
        // Get the context wrapper
        ContextWrapper wrapper = new ContextWrapper(getApplicationContext());

        // Initialize a new file instance to save bitmap object
        File file = wrapper.getDir("Images", Context.MODE_PRIVATE);
        file = new File(file,"${UUID.randomUUID()}.jpg");

        try{
            // Compress the bitmap and save in jpg format
            OutputStream stream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
            stream.flush();
            stream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        Log.v("ST_ANTHONY", file.getAbsolutePath());
        // Return the saved bitmap uri
        //return Uri.parse(file.absolutePath)
        return file;
    }

    private void sendOnServer(final CreateDataModel data, final boolean isDone, int dataPosition) {
        File img1File = null;
        File img2File = null;
        File img3File = null;
        File img4File = null;

        String img1Url = "";
        String img2Url = "";
        String img3Url = "";
        String img4Url = "";

        List<Uri> fileList = new ArrayList<>();

        String file1Name = data.getPropertyImageOne();
        String file2Name = data.getPropertyImageTwo();
        String file3Name = data.getPropertyImageThree();
        String file4Name = data.getPropertyImageFour();

        if (file1Name!=null && !file1Name.isEmpty()){
            img1File = new File(file1Name);
            Uri filePath = Uri.fromFile(img1File);
            fileList.add(filePath);
            //uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }
        if (file2Name!=null && !file2Name.isEmpty()){
            img2File = new File(file2Name);
            Uri filePath = Uri.fromFile(img2File);
            fileList.add(filePath);
           // uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }
        if (file3Name!=null && !file3Name.isEmpty()){
            img3File = new File(file3Name);
            Uri filePath = Uri.fromFile(img3File);
            fileList.add(filePath);
            //uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }
        if (file4Name!=null && !file4Name.isEmpty()){
            img4File = new File(file4Name);
            Uri filePath = Uri.fromFile(img3File);
            fileList.add(filePath);
            //uploadImageFile(data.getWard()+"_"+data.getPropertyNo(), filePath);
        }

        if (NetworkHandler.isNetworkAvailable(mContext)) {
            uploadImageFile(data.getWard()+"_"+data.getPropertyNo()+"_"+userId+"_"+data.getId()+"_", fileList, 0, isDone, dataPosition);
        } else {
            Toast.makeText(mContext, "No Network", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(mContext, NumberingActivity.class));
            finish();
        }

    }

    private void updateData(CreateDataModel data) {
        try {
            Dao<CreateDataModel,Integer> updateDao=getHelper().getDao(CreateDataModel.class);
            UpdateBuilder<CreateDataModel, Integer> updateBuilder = updateDao.updateBuilder();
            updateBuilder.updateColumnValue("synced", true);
            updateBuilder.where().eq("id", data.getId());
            int isUpdated = updateBuilder.update();
            Log.v("UPDATED", "update value :" + isUpdated);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showData(){
        CreateDataModel data = dataList.get(dataList.size()-1);
        String allDataValue = "Ward : "+data.getWard()+"\nZone : "+data.getZone()+"\nBlock : "+data.getBlock()
                +"\nRegistration Number : "+data.getPropertyNo()+"\nName : "+data.getPropertyName()+"\nAddress : "
                +data.getPropertyAddress()+"\nLatitude : "+data.getPropertyLatitude()+"\nLongitude : "
                +data.getPropertyLongitude()+"\nImage 1 : "+data.getPropertyImageOne()+"\nImage 2 : "+data.getPropertyImageTwo();
        Toast.makeText(mContext, allDataValue, Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llRoot:
                CreateDataModel myData = (CreateDataModel) v.getTag();
                startActivity(new Intent(mContext, PropertyDetailActivity.class).putExtra("DATA_MODEL", myData));
                break;
        }
    }

    private void uploadImageFile(final String img_name, final List<Uri> fileList, final int position, final boolean isDone, final int dataPosition) {

        Uri filePath = fileList.get(position);

            if (filePath != null) {
                int imgCount = position+1;
                StorageReference sRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS + img_name+imgCount + "." + "jpg");

                sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            int pos = position;
                            pos++;

                            //Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                            imageUrl = taskSnapshot.getDownloadUrl().toString();
                            switch (pos){
                                case 1 :
                                   imgWeb1 = imageUrl;
                                break;
                                case 2 :
                                   imgWeb2 = imageUrl;
                                break;
                                case 3 :
                                   imgWeb3 = imageUrl;
                                break;
                                case 4 :
                                   imgWeb4 = imageUrl;
                                break;
                            }
                            if (pos<fileList.size()){
                                uploadImageFile(img_name, fileList, pos, isDone, dataPosition);
                            }else {
                                uploadFinalDataToServer(isDone, dataPosition);
                                //Toast.makeText(mContext, "All image Uploaded", Toast.LENGTH_SHORT).show();
                            }
                            Log.v("IMG_WEB_URI", imageUrl);
                        }

                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //progressDialog.dismiss();
                            AppProgressDialog.show(mDialog);
                            startActivity(new Intent(mContext, NumberingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            //display an error if no file is selected
        }
    }

    private void uploadFinalDataToServer(final boolean isDone, final int dataPosition){
        final CreateDataModel data = dataList.get(dataPosition-1);

        String wardNamePart = data.getWard();
        String zoneNamePart = data.getZone();
        String blockNamePart = data.getBlock();
        String propertyNumberPart = data.getPropertyNo();
        String propertyNamePart = data.getPropertyName();
        String addressPart = data.getPropertyAddress();

        String cpnPart = data.getCorporationPropertyNumber();
        String putPart = data.getPropertyUsageType();
        String ptPart = data.getPropertyType();
        String rCountPart = data.getResidentialCount();
        String cCountPart = data.getCommercialCount();
        String r1Part = data.getRemarkOne();
        String r2Part = data.getRemarkTwo();
        String deviceId = String.valueOf(data.getId());

        String latPart = String.valueOf(data.getPropertyLatitude());
        String lngPart = String.valueOf(data.getPropertyLongitude());

        if (NetworkHandler.isNetworkAvailable(mContext)){

            RetrofitService.getServerResponse(new Dialog(mContext), retrofitApiClient.uploadPropertyDataToServer(imgWeb1,
                    imgWeb2, imgWeb3, imgWeb4, wardNamePart, zoneNamePart, blockNamePart, propertyNumberPart,
                    propertyNamePart, addressPart, cpnPart, putPart, ptPart, rCountPart, cCountPart, r1Part, r2Part,
                    deviceId, lngPart, latPart, userId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ResponseBody response = (ResponseBody) result.body();
                    try {
                        JSONObject jsonObject = new JSONObject(response.string());
                        if (jsonObject.getBoolean("status")){
                            Toast.makeText(mContext, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            updateData(data);
                            if (isDone){
                                AppProgressDialog.hide(mDialog);
                                startActivity(new Intent(mContext, NumberingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                finish();
                            }else{
                                prepareDataForServer(dataPosition);
                            }
                        }else{
                            Toast.makeText(mContext, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(mContext, NumberingActivity.class));
                            finish();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        startActivity(new Intent(mContext, NumberingActivity.class));
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        startActivity(new Intent(mContext, NumberingActivity.class));
                        finish();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(mContext, NumberingActivity.class));
                    finish();
                }
            });
        }else{
            Toast.makeText(mContext, "No Network", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(mContext, NumberingActivity.class));
            finish();
        }

        //prepareDataForServer(dataPosition);
    }


    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    /*************************************************************
     * ***********************************************************/

    /*************************************************************
     * ImageName ==>> WARD_ZONE_PROPERTY_NUMBER_1 or
     *                WARD_ZONE_PROPERTY_NUMBER_2 or
     *                WARD_ZONE_PROPERTY_NUMBER_3 or
     *                WARD_ZONE_PROPERTY_NUMBER_4 or
     **************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(NumberingActivity.this,ContentPageActivity.class));
        finish();
    }
}
